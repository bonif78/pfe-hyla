--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

-- Started on 2021-02-04 15:42:00

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3067 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 24769)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    iduser integer NOT NULL,
    namefirst character varying(50) NOT NULL,
    namelast character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    password character varying(256) NOT NULL,
    idrole integer NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24772)
-- Name: account_idRole_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."account_idRole_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."account_idRole_seq" OWNER TO postgres;

--
-- TOC entry 3068 (class 0 OID 0)
-- Dependencies: 201
-- Name: account_idRole_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."account_idRole_seq" OWNED BY public."user".idrole;


--
-- TOC entry 202 (class 1259 OID 24774)
-- Name: account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_id_seq OWNER TO postgres;

--
-- TOC entry 3069 (class 0 OID 0)
-- Dependencies: 202
-- Name: account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_id_seq OWNED BY public."user".iduser;


--
-- TOC entry 203 (class 1259 OID 24776)
-- Name: equipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.equipe (
    idequipe integer NOT NULL,
    name character varying(50) NOT NULL,
    adminclient integer NOT NULL
);


ALTER TABLE public.equipe OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 24779)
-- Name: equipes_column1_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.equipes_column1_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipes_column1_seq OWNER TO postgres;

--
-- TOC entry 3070 (class 0 OID 0)
-- Dependencies: 204
-- Name: equipes_column1_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.equipes_column1_seq OWNED BY public.equipe.idequipe;


--
-- TOC entry 205 (class 1259 OID 24781)
-- Name: provider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.provider (
    idprovider integer NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.provider OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 24784)
-- Name: provider_credentials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.provider_credentials (
    idprovider_credential integer NOT NULL,
    idprovider integer NOT NULL,
    username character varying(50),
    email character varying(50) NOT NULL,
    password character varying(256) NOT NULL,
    description text,
    idcredentialext character varying(128)
);


ALTER TABLE public.provider_credentials OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 24790)
-- Name: providerCredentials_id_provider_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."providerCredentials_id_provider_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."providerCredentials_id_provider_seq" OWNER TO postgres;

--
-- TOC entry 3071 (class 0 OID 0)
-- Dependencies: 207
-- Name: providerCredentials_id_provider_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."providerCredentials_id_provider_seq" OWNED BY public.provider_credentials.idprovider;


--
-- TOC entry 208 (class 1259 OID 24792)
-- Name: providerCredentials_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."providerCredentials_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."providerCredentials_id_seq" OWNER TO postgres;

--
-- TOC entry 3072 (class 0 OID 0)
-- Dependencies: 208
-- Name: providerCredentials_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."providerCredentials_id_seq" OWNED BY public.provider_credentials.idprovider_credential;


--
-- TOC entry 209 (class 1259 OID 24794)
-- Name: provider_equipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.provider_equipe (
    id_equipe integer NOT NULL,
    id_provider_credentials integer NOT NULL
);


ALTER TABLE public.provider_equipe OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 24797)
-- Name: providerUser_id_account_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."providerUser_id_account_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."providerUser_id_account_seq" OWNER TO postgres;

--
-- TOC entry 3073 (class 0 OID 0)
-- Dependencies: 210
-- Name: providerUser_id_account_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."providerUser_id_account_seq" OWNED BY public.provider_equipe.id_equipe;


--
-- TOC entry 211 (class 1259 OID 24799)
-- Name: providerUser_id_providerCredentials_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."providerUser_id_providerCredentials_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."providerUser_id_providerCredentials_seq" OWNER TO postgres;

--
-- TOC entry 3074 (class 0 OID 0)
-- Dependencies: 211
-- Name: providerUser_id_providerCredentials_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."providerUser_id_providerCredentials_seq" OWNED BY public.provider_equipe.id_provider_credentials;


--
-- TOC entry 212 (class 1259 OID 24801)
-- Name: provider_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provider_id_seq OWNER TO postgres;

--
-- TOC entry 3075 (class 0 OID 0)
-- Dependencies: 212
-- Name: provider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.provider_id_seq OWNED BY public.provider.idprovider;


--
-- TOC entry 213 (class 1259 OID 24803)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    idrole integer NOT NULL,
    name character varying(250),
    privilegelevel integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 24807)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- TOC entry 3076 (class 0 OID 0)
-- Dependencies: 214
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.idrole;


--
-- TOC entry 215 (class 1259 OID 24809)
-- Name: userequipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.userequipe (
    idequipe integer NOT NULL,
    iduser integer NOT NULL
);


ALTER TABLE public.userequipe OWNER TO postgres;

--
-- TOC entry 2892 (class 2604 OID 24812)
-- Name: equipe idequipe; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipe ALTER COLUMN idequipe SET DEFAULT nextval('public.equipes_column1_seq'::regclass);


--
-- TOC entry 2893 (class 2604 OID 24813)
-- Name: provider idprovider; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider ALTER COLUMN idprovider SET DEFAULT nextval('public.provider_id_seq'::regclass);


--
-- TOC entry 2894 (class 2604 OID 24814)
-- Name: provider_credentials idprovider_credential; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_credentials ALTER COLUMN idprovider_credential SET DEFAULT nextval('public."providerCredentials_id_seq"'::regclass);


--
-- TOC entry 2896 (class 2604 OID 24815)
-- Name: role idrole; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN idrole SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- TOC entry 2891 (class 2604 OID 24816)
-- Name: user iduser; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN iduser SET DEFAULT nextval('public.account_id_seq'::regclass);


--
-- TOC entry 3049 (class 0 OID 24776)
-- Dependencies: 203
-- Data for Name: equipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.equipe VALUES (6, 'PFE Hyla Back', 5);
INSERT INTO public.equipe VALUES (3, 'NexWorld', 2);
INSERT INTO public.equipe VALUES (1, 'PFE Hyla Front', 6);
INSERT INTO public.equipe VALUES (4, 'Efrei Paris', 5);


--
-- TOC entry 3051 (class 0 OID 24781)
-- Dependencies: 205
-- Data for Name: provider; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.provider VALUES (1, 'aws');
INSERT INTO public.provider VALUES (3, 'Azure');


--
-- TOC entry 3052 (class 0 OID 24784)
-- Dependencies: 206
-- Data for Name: provider_credentials; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.provider_credentials VALUES (1, 1, 'aws_groupe1', 'email.aws@gmail.com', 'admin', 'as web sservices', 'jfdgfhdgfhd');
INSERT INTO public.provider_credentials VALUES (3, 3, 'azure_groupe1', 'system.aws@gmail.com', 'system', 'credential provider aws for the intern project', 'JAPsdh25441dasds55s');
INSERT INTO public.provider_credentials VALUES (6, 3, 'azure_groupe2', 'demo.aws@gmail.com', 'demopwd', 'as demo sservices', 'vfihnxwe6Cghg2lJF');


--
-- TOC entry 3055 (class 0 OID 24794)
-- Dependencies: 209
-- Data for Name: provider_equipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.provider_equipe VALUES (1, 1);
INSERT INTO public.provider_equipe VALUES (6, 1);
INSERT INTO public.provider_equipe VALUES (4, 3);
INSERT INTO public.provider_equipe VALUES (4, 1);
INSERT INTO public.provider_equipe VALUES (4, 6);


--
-- TOC entry 3059 (class 0 OID 24803)
-- Dependencies: 213
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.role VALUES (1, 'adminglobal', 1);
INSERT INTO public.role VALUES (3, 'Admin Clients', 2);
INSERT INTO public.role VALUES (5, 'adminsystem', 1);
INSERT INTO public.role VALUES (7, 'user', 1);


--
-- TOC entry 3046 (class 0 OID 24769)
-- Dependencies: 200
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."user" VALUES (2, 'francois', 'boni', 'fboni.78@gmail.com', 'admin', 1);
INSERT INTO public."user" VALUES (5, 'mathilde', 'kerdiles', 'mathilde.user@hyla.fr', 'mot de passe', 3);
INSERT INTO public."user" VALUES (13, 'demo2', 'doc', 'demo.doc@gmail.com', 'test', 1);
INSERT INTO public."user" VALUES (6, 'Begeot', 'HugueS', 'Huges.bg@efrei.net', '12345678', 1);
INSERT INTO public."user" VALUES (9, 'celia', 'bunouf', 'celia.user@hyla.fr', 'mot de passe', 5);


--
-- TOC entry 3061 (class 0 OID 24809)
-- Dependencies: 215
-- Data for Name: userequipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.userequipe VALUES (1, 5);
INSERT INTO public.userequipe VALUES (6, 9);
INSERT INTO public.userequipe VALUES (6, 5);
INSERT INTO public.userequipe VALUES (4, 9);
INSERT INTO public.userequipe VALUES (4, 5);
INSERT INTO public.userequipe VALUES (4, 2);
INSERT INTO public.userequipe VALUES (1, 6);
INSERT INTO public.userequipe VALUES (1, 9);
INSERT INTO public.userequipe VALUES (1, 13);
INSERT INTO public.userequipe VALUES (3, 6);
INSERT INTO public.userequipe VALUES (3, 2);
INSERT INTO public.userequipe VALUES (3, 5);
INSERT INTO public.userequipe VALUES (3, 9);
INSERT INTO public.userequipe VALUES (3, 13);
INSERT INTO public.userequipe VALUES (1, 2);


--
-- TOC entry 3077 (class 0 OID 0)
-- Dependencies: 201
-- Name: account_idRole_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."account_idRole_seq"', 1, false);


--
-- TOC entry 3078 (class 0 OID 0)
-- Dependencies: 202
-- Name: account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_id_seq', 15, true);


--
-- TOC entry 3079 (class 0 OID 0)
-- Dependencies: 204
-- Name: equipes_column1_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.equipes_column1_seq', 10, true);


--
-- TOC entry 3080 (class 0 OID 0)
-- Dependencies: 207
-- Name: providerCredentials_id_provider_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."providerCredentials_id_provider_seq"', 1, false);


--
-- TOC entry 3081 (class 0 OID 0)
-- Dependencies: 208
-- Name: providerCredentials_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."providerCredentials_id_seq"', 6, true);


--
-- TOC entry 3082 (class 0 OID 0)
-- Dependencies: 210
-- Name: providerUser_id_account_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."providerUser_id_account_seq"', 1, false);


--
-- TOC entry 3083 (class 0 OID 0)
-- Dependencies: 211
-- Name: providerUser_id_providerCredentials_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."providerUser_id_providerCredentials_seq"', 1, false);


--
-- TOC entry 3084 (class 0 OID 0)
-- Dependencies: 212
-- Name: provider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.provider_id_seq', 4, true);


--
-- TOC entry 3085 (class 0 OID 0)
-- Dependencies: 214
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 7, true);


--
-- TOC entry 2900 (class 2606 OID 24818)
-- Name: equipe equipe_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipe
    ADD CONSTRAINT equipe_pk PRIMARY KEY (idequipe);


--
-- TOC entry 2906 (class 2606 OID 24820)
-- Name: role pk_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT pk_id PRIMARY KEY (idrole);


--
-- TOC entry 2898 (class 2606 OID 24822)
-- Name: user pk_id2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT pk_id2 PRIMARY KEY (iduser);


--
-- TOC entry 2902 (class 2606 OID 24824)
-- Name: provider pk_id3; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider
    ADD CONSTRAINT pk_id3 PRIMARY KEY (idprovider);


--
-- TOC entry 2904 (class 2606 OID 24826)
-- Name: provider_credentials pk_id4; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_credentials
    ADD CONSTRAINT pk_id4 PRIMARY KEY (idprovider_credential);


--
-- TOC entry 2908 (class 2606 OID 24828)
-- Name: role un_name; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT un_name UNIQUE (name);


--
-- TOC entry 2910 (class 2606 OID 24829)
-- Name: equipe equipe_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipe
    ADD CONSTRAINT equipe_fk FOREIGN KEY (adminclient) REFERENCES public."user"(iduser);


--
-- TOC entry 2911 (class 2606 OID 24834)
-- Name: provider_credentials fk_provider; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_credentials
    ADD CONSTRAINT fk_provider FOREIGN KEY (idprovider) REFERENCES public.provider(idprovider) MATCH FULL;


--
-- TOC entry 2912 (class 2606 OID 24839)
-- Name: provider_equipe fk_providerCredentials; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_equipe
    ADD CONSTRAINT "fk_providerCredentials" FOREIGN KEY (id_provider_credentials) REFERENCES public.provider_credentials(idprovider_credential) MATCH FULL;


--
-- TOC entry 2909 (class 2606 OID 24844)
-- Name: user fk_role; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT fk_role FOREIGN KEY (idrole) REFERENCES public.role(idrole) MATCH FULL ON DELETE SET NULL;


--
-- TOC entry 2913 (class 2606 OID 24849)
-- Name: provider_equipe providerequipe_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_equipe
    ADD CONSTRAINT providerequipe_fk FOREIGN KEY (id_equipe) REFERENCES public.equipe(idequipe);


--
-- TOC entry 2914 (class 2606 OID 24854)
-- Name: userequipe userequipe_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.userequipe
    ADD CONSTRAINT userequipe_fk FOREIGN KEY (idequipe) REFERENCES public.equipe(idequipe);


--
-- TOC entry 2915 (class 2606 OID 24859)
-- Name: userequipe userequipe_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.userequipe
    ADD CONSTRAINT userequipe_fk_1 FOREIGN KEY (iduser) REFERENCES public."user"(iduser);


-- Completed on 2021-02-04 15:42:00

--
-- PostgreSQL database dump complete
--

