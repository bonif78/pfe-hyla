## PFE Hyla

## Sommaire
**[Les Grandes Lignes](https://gitlab.com/bonif78/pfe-hyla#les-grandes-lignes)**

[Où trouver le projet](https://gitlab.com/bonif78/pfe-hyla#ou-trouver-le-projet)

[Le concept du projet](https://gitlab.com/bonif78/pfe-hyla#le-concept-du-projet)

[Bien commencer](https://gitlab.com/bonif78/pfe-hyla#bien-commencer)


**[Présentation Avancée](https://gitlab.com/bonif78/pfe-hyla#presentation-avancee)**

[Synopsis](https://gitlab.com/bonif78/pfe-hyla#synopsis)

[Conception](https://gitlab.com/bonif78/pfe-hyla#conception)

[RoadMap](https://gitlab.com/bonif78/pfe-hyla#roadMap)

[Prototype](https://gitlab.com/bonif78/pfe-hyla#prototype)


**[Fiche technique](https://gitlab.com/bonif78/pfe-hyla#fiche-technique)**


**[Auteurs](https://gitlab.com/bonif78/pfe-hyla#auteurs)**




## Les Grandes Lignes

### Où trouver le projet
Voici le dépôt GitLab du projet :
 ```
https://gitlab.com/bonif78/pfe-hyla
 ```



### Le concept du projet
Ce projet est un outil FinOps cherchant à monitorer la consommation des outils en temps réel, connectés aux différents services Cloud, capable de désactiver les ressources non utilisées pour réduire, voire supprimer les frais inutiles.

Cet outil a 3 objectifs :
* Créer une API générique qui puisse s'adapter aux fournisseurs Cloud (AWS, Azure...)
* Avoir des scripts pour désactiver les outils automatiquement comme sur demande
* Une solution "Multi-tenantes" ; sur un même moteur, les rôles différents accèdent à des données différentes

Les différents modules qui communiquent avec les services Cloud sont indépendants les uns des autres, afin d'éviter tout dysfonctionnement en cascade.


### Bien commencer
Voici les étapes à suivre pour lancer le projet, en 3 étapes :

```
BASE DE DONNÉES :
Créer une base de données PostGres s'appelant "testHyla".
Utiliser les scripts suivants pour créer un bdd appelée 'testHyla' :
- bdd/bdd-sql-pfe-hyla.sql

BACK-END :
Tout d'abord, il faut définir le fichier de configuration :
Créer une copie du fichier "hyla-back-end/DBUserManagement/src/main/resources/hibernate-template.cfg.xml"
Le renommer "hyla-back-end/DBUserManagement/src/main/resources/hibernate.cfg.xml"
Configurer les champs "connection.username" et "connection.password" selon vos credentials PostGres.

Ensuite, il faut configurer les projets : sélectionnez-en un, puis intégrer les 2 en importants les fichiers Maven.
Commencez obligatoirement par lancer le projet "ServiceDiscoveryApplication", puis lancer les 2 autres dans l'ordre de votre choix.


FRONT-END :
Afin de télécharger les dépendances manquantes, ouvrez une console de commande dans le projet Front et entrez :
  $npm install
  
Ensuite, pour rendre le serveur opérationnel :
  $npm run dev
```



## Présentation Avancée 


### Synopsis
Comme mentionné plus haut, ce projet est un outil FinOps cherchant à monitorer la consommation de services Cloud en temps réel.

On peut pour cela diviser le projet en différente sous-partie : la gestion de comptes avec droits d'accès hiérarchisés, le stockage de credentials pour accéder aux services Cloud et les outils de monitoring de ces logiciels, ainsi qu'un interface web.

![schema n° 1 : organization graph](https://gitlab.com/bonif78/pfe-hyla/-/blob/opsilonn-master-patch-78510/readMe-assets/schema-organisation.png)

De manière synthétique :

#### Gestion de comptes
* Management d'équipe et de comptes utilisateurs
  * créer des équipes
  * créer des comptes utilisateurs
* Manager les rôles au sein d'une équipe
  * affilier n utilisateurs à une équipe
  * relier chaque équipe à un client
  * manager les rôles au sein d'une équipe

#### Services Cloud
* Référencer les différents services Cloud
  * avoir une représentation non relationnelle de chaque provider
  * avoir une représentation non relationnelle de chaque service
  * définir des credentials partagés au sein d'une équipe
* Surveiller la consommation des services Cloud
  * définir et stocker la représentation de cette consommation, par service
  
#### API
* Construire une API
  * Faire la jonction entre les comptes équipes et les credentials des services Cloud
  * API centralisée, End-points pour retourner les informations des comptes / utilisations / credentials
  * avoir un accès conditionné par le rôle de l'utilisateur (droits hiérarchisés)

#### Front-end
* interface user-friendly pour accéder à l'API
  
Pour mieux représenter les interactions entre Back-End et Front-End, considérons que l'API fait la jonction, d'une part entre le Front, d'autres part avec les différentes bases de données et les différents services Cloud.

![schema n° 2 : user graph](https://gitlab.com/bonif78/pfe-hyla/-/blob/opsilonn-master-patch-78510/readMe-assets/schema-back-front.png)


### Conception
Pour implémenter ce système, il nous faut tout d'abord 2 bases de données, l'une relationnelle et l'autre non, qui communiqueront avec l'API :
* **[PostGres](https://www.postgresql.org)** - SQL database, for regular tables such as accounts
* **[MongoDB](https://www.mongodb.com/fr)** - NoSQL database, for varying collections such as connections to Cloud services

Un serveur Java sera implémenté pour centraliser toutes les connections sous une API respectant le modèle RESTful.

Pour ce qui est du Front, nous avons tout d'abord élaboré un Figma pour s'accorder sur l'interface, puis nous l'avons développé en **[Vue.js](https://fr.vuejs.org)**.



### RoadMap

* Phase 1 : configuration
  * modélisation et création des bases de données
  * configuration du serveur Java
  * planification du Front-End sur Figma

* Phase 2 : élaboration de l'API 
  * API
    * CRUD comptes utilisateurs
    * CRUD credentials
    * CRUD services Cloud
  * FRONT
    * finalisation et exportation du modèle Figma

* Phase 3 : continuation de l'API et implémentation du système de compte côté Front
  * API
    * gestion des ressources Cloud
    * gestion des facturations
  * FRONT
    * login / logout
    * Mot de passe oublié
    * Modification des données utilisateur

* Phase 4 : continuation
  * API
    * taux de consommation
    * sécurisation (session, login)
  * FRONT
    * dashboard
    * gérer provider
    * gérer équipe d'utilisateurs
    * gérer compte client

* Phase 5 : configuration
  * API
    * vérification des privilèges
    * planification des extinctions des ressources
    * instauration de règles pour les ressources
  * FRONT
    * détail provider
    * historique
    * extinction de services
    * évaluation des coûts futurs

* Phase 6 : finalisation
  * API
    * activation / désactivation des comptes
  * déploiement



### Prototype
Le prototype permettra la création d'équipes et de comptes utilisateurs parmi elles : client, chef, admin, développeur... chaque rôle a accès à des droits et des prérogatives qui leurs sont propres.
Le serveur permet la connection à ces comptes ainsi qu'à des services Cloud, dont les credentials sont partagés au sein d'une équipe. Les ressources peuvent être monitorées par une équipe, et éteintes si besoin est.
L'utilisateur accède à ces services via une interface web.




## Fiche technique
Le projet sera construit avec :

BACK-END
* **[Java 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)** - version de Java utilisée
* **[Tomcat 9](https://tomcat.apache.org/download-90.cgi)** - serveur pour centraliser les API
* **[Intelij](https://www.jetbrains.com/idea)** - IDE utilisé pour le Back
* **[PostGres](https://www.postgresql.org)** - base de données SQL, pour des tables régulières telles des comptes utilisateurs
* **[MongoDB](https://www.mongodb.com/fr)** - base de données NoSQL, pour des collections variées telles que les connections aux services Cloud services

FRONT-END
* **[Node.js](https://nodejs.org)** - Un framework Javascript de gestion asynchrone des événements conçu pour construire des applications web modulables
* **[Vue.js](https://fr.vuejs.org)** - Framework fondé sur **[Node.js](https://nodejs.org)**
* **[Vuetify](https://vuetifyjs.com)** - Bibliothèque UI pour **[Vue.js](https://fr.vuejs.org)**
* **[Nuxt](https://nuxtjs.org)** - Framework **[Vue.js](https://fr.vuejs.org)** pour concevoir des applications singlepage
* **[Axios](https://github.com/axios)** - Client HTTP reposant sur des Promesses pour **[Node.js](https://nodejs.org)**
* **[Vuex](https://vuex.vuejs.org)** -  Management d'état et librairie pour **[Vue.js](https://fr.vuejs.org)**



## Auteurs
Ce projet a été fait par les membress suivants de l'Efrei Paris :
* **BEGEOT Hugues**
    * [GitHub](https://github.com/opsilonn)
    * [GitLab](https://gitlab.com/opsilonn)
* **BONI François**
    * [GitHub](https://github.com/scorpionsdu78)
    * [GitLab](https://gitlab.com/bonif78)
* **BUNOUF Célia**
    * [GitHub](https://github.com/Cvsdm)
    * [GitLab](https://gitlab.com/Cvsdm)
* **EL-HADDAD Céline**
    * [GitHub](https://github.com/Ceeline)
    * [GitLab](https://gitlab.com/Ceeline)
* **KERDILÈS Mathilde**
    * [GitHub](https://github.com/Armalyca)
    * [GitLab](https://gitlab.com/Armalyca)
* **LEPRÉ Paul**
    * [GitHub](https://github.com/paul-lepre)
    * [GitLab](https://gitlab.com/Paul_lepre)

Aussi, la liste des [membres](https://gitlab.com/bonif78/pfe-hyla/-/project_members) ayant participé à ce projet.

Note : nous sommes actuellement dans notre 5ème année (2020-21) d'un cursus de Software Engineering.
