package com.hyla.RestExpose.service.api;


import com.hyla.RestExpose.service.DTO.RoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping("/roles")
public class RoleService {

    @Autowired
    RestTemplate restTemplate;

    /**
     * RoleDTO all the information of the role
     * return all the roles
     * @return RoleDTO[]
     */
    @GetMapping
    @ResponseBody
    public RoleDTO[] GetAllRoles () {
        try{
            ResponseEntity<RoleDTO[]> roles = restTemplate.getForEntity("http://userDBManagement/roles", RoleDTO[].class);
            return roles.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * return information of one role
     * @param id the id of the role
     * @return RoleDTO
     */
    @GetMapping("/{id}")
    @ResponseBody
    public RoleDTO GetRoleByID (@PathVariable("id") int id) {
        try{
            return (RoleDTO) restTemplate.getForObject("http://userDBManagement/roles/" + id, RoleDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * add a new role
     * @param role the role to add
     * @return RoleDTO
     * return the newly created role
     */
    @PostMapping
    @ResponseBody
    public RoleDTO InsertRole (@RequestBody RoleDTO role) {
        try{
            return (RoleDTO) restTemplate.postForObject("http://userDBManagement/roles/" , role, RoleDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * update one role
     * @param roleDTO the information to update
     * @param id the id of the role to update
     * @return RoleDTO
     * return the newly updated role
     */
    @PutMapping("/{id}")
    @ResponseBody
    public RoleDTO UpdateRole (@RequestBody RoleDTO roleDTO, @PathVariable("id") int id) {
        try{
            HttpEntity<RoleDTO> roleDTOHttpEntity = new HttpEntity<>(roleDTO);
            HttpEntity<RoleDTO> result =  restTemplate.exchange("http://userDBManagement/roles/" + id, HttpMethod.PUT, roleDTOHttpEntity, RoleDTO.class);
            return (RoleDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    @DeleteMapping("/{id}")
    public boolean DeleteRole (@PathVariable("id") int id) {
        try {
            restTemplate.delete("http://userDBManagement/roles/" + id);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
