package com.hyla.RestExpose.service.api;

import com.hyla.RestExpose.service.DTO.BillingDTO;
import com.hyla.RestExpose.service.DTO.RuleDTO;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/assets")
public class AssetService {

    /**
     * @param id of the ressource
     * @return the lists of the billing of the ressource
     * @throws FileNotFoundException if there is no billing for this ressource
     */
    @GetMapping("/{id}/billing")
    @ResponseBody
    public List<BillingDTO> GetAssets (@PathVariable("id") String id)  {
        List<BillingDTO> assets = new ArrayList<>();

        String filename =  id + ".csv";


        try {
            File file = new File(getClass().getClassLoader().getResource(filename).getFile());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");
                BillingDTO billingDTO = new BillingDTO(values[0], Float.parseFloat(values[1]));
                assets.add(billingDTO);

            }

            return assets;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
        }
        return assets;
    }

    @GetMapping("/{id}/rules")
    @ResponseBody
    public List<RuleDTO> getRules (@PathVariable("id") String id) {
        List<RuleDTO> rules = new ArrayList<>();

        String filename = "rule-" + id + ".csv";

        try {
            File file = new File(getClass().getClassLoader().getResource(filename).getFile());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");
                RuleDTO ruleDTO = new RuleDTO(Integer.parseInt(values[0]), id, values[1], values[2], Boolean.parseBoolean(values[3]));
                rules.add(ruleDTO);

            }

            return rules;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
        }

        return new ArrayList<>();
    }
}
