package com.hyla.RestExpose.service.DTO;


public class EquipesDTO {
    private int idequipe;
    private String name;

    private UserDTO adminClient;



    public EquipesDTO(int idequipe, String name, UserDTO adminClient) {
        this.idequipe = idequipe;
        this.name = name;
        this.adminClient = adminClient;
    }

    public EquipesDTO() {
    }

    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(UserDTO adminClient) {
        this.adminClient = adminClient;
    }
}
