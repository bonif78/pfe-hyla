package com.hyla.RestExpose.service.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDTO {
    private int idrole;
    private String name;
    private int privilegeLevel;

    public RoleDTO(int idrole, String name, int privilegeLevel) {
        this.idrole = idrole;
        this.name = name;
        this.privilegeLevel = privilegeLevel;
    }

    public RoleDTO() {
    }

    public int getIdrole() {
        return idrole;
    }

    public void setIdrole(int idrole) {
        this.idrole = idrole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrivilegeLevel() {
        return privilegeLevel;
    }

    public void setPrivilegeLevel(int privilegeLevel) {
        this.privilegeLevel = privilegeLevel;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
                "idrole=" + idrole +
                '}';
    }
}
