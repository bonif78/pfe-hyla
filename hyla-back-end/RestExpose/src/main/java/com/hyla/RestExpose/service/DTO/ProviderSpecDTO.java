package com.hyla.RestExpose.service.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hyla.RestExpose.controler.AssetControler;
import com.hyla.RestExpose.service.api.AssetService;
import com.hyla.RestExpose.service.api.ProviderCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class ProviderSpecDTO {
    private int idprovider_credential;
    private String username;
    private String email;

    private String idcredentialext;
    private List<AssetDTO> services;


    public ProviderSpecDTO(ProviderCredentialsLightDTO providerCredentialsLightDTO) {
        AssetControler assetControler = new AssetControler();
        this.idprovider_credential = providerCredentialsLightDTO.getIdprovider_credential();
        this.username = providerCredentialsLightDTO.getUsername();
        this.email = providerCredentialsLightDTO.getEmail();

        this.idcredentialext = providerCredentialsLightDTO.getIdcredentialext();
        try {
            this.services = assetControler.GetAssets(this.idprovider_credential);
        } catch (Exception e) {
            this.services = new ArrayList<>();
        }

    }

    public ProviderSpecDTO(ProviderCredentialsDTO providerCredentialsDTO) {
        AssetControler assetControler = new AssetControler();
        this.idprovider_credential = providerCredentialsDTO.getIdprovider_credential();
        this.username = providerCredentialsDTO.getUsername();
        this.email = providerCredentialsDTO.getEmail();
        this.idcredentialext = providerCredentialsDTO.getIdcredentialext();
        try {
            this.services = assetControler.GetAssets(this.idprovider_credential);
        } catch (Exception e) {
            this.services = new ArrayList<>();
        }
    }

    public int getIdprovider_credential() {
        return idprovider_credential;
    }

    public void setIdprovider_credential(int idprovider_credential) {
        this.idprovider_credential = idprovider_credential;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdcredentialext() {
        return idcredentialext;
    }

    public void setIdcredentialext(String idcredentialext) {
        this.idcredentialext = idcredentialext;
    }

    public List<AssetDTO> getServices() {
        return services;
    }

    public void setServices(List<AssetDTO> services) {
        this.services = services;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
