package com.hyla.RestExpose;

import com.hyla.RestExpose.service.error.RestTemplateResponseErrorHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class RestExposeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestExposeApplication.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate () {
		RestTemplate restTemplate = new RestTemplate();
		// restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
		return restTemplate;
	}
}
