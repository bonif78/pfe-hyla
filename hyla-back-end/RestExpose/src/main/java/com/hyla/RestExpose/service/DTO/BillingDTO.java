package com.hyla.RestExpose.service.DTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class BillingDTO {
    private LocalDate date;
    private float amount;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public BillingDTO() {
    }

    public BillingDTO(String date, float amount) {
        this.date = LocalDate.parse(date, formatter);
        this.amount = amount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "BillingDTO{" +
                "date=" + date +
                ", amount=" + amount +
                '}';
    }
}
