package com.hyla.RestExpose.service.api;


import com.hyla.RestExpose.service.DTO.ProviderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/providers")
public class ProviderService {

    @Autowired
    RestTemplate restTemplate;

    /**
     * return all the provider
     * @return ProviderDTO[]
     */
    @GetMapping
    @ResponseBody
    public ProviderDTO[] GetAllProviders () {
        try{
            ResponseEntity<ProviderDTO[]> providers = restTemplate.getForEntity("http://userDBManagement/provider", ProviderDTO[].class);
            return providers.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * @param id of the provider
     * @return ProviderDTO
     * only the provider of the id
     */
    @GetMapping("/{id}")
    @ResponseBody
    public ProviderDTO GetProviderByID (@PathVariable("id") int id) {
        try{
            return (ProviderDTO) restTemplate.getForObject("http://userDBManagement/provider/" + id, ProviderDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * add a new provider
     * @param provider the provider to add
     * @return ProviderDTO
     * the provider added in the database
     */
    @PostMapping
    @ResponseBody
    public ProviderDTO InsertProvider (@RequestBody ProviderDTO provider) {
        try{
            return (ProviderDTO) restTemplate.postForObject("http://userDBManagement/provider" , provider, ProviderDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * modify a provider
     * @param providerDTO the modification of the provider
     * @param id the id to the provider to change
     * @return ProviderDTO
     * the newly updated provider
     */
    @PutMapping("/{id}")
    @ResponseBody
    public ProviderDTO UpdateProvider (@RequestBody ProviderDTO providerDTO, @PathVariable("id") int id) {
        try{
            HttpEntity<ProviderDTO> roleDTOHttpEntity = new HttpEntity<>(providerDTO);
            HttpEntity<ProviderDTO> result =  restTemplate.exchange("http://userDBManagement/provider/" + id, HttpMethod.PUT, roleDTOHttpEntity, ProviderDTO.class);
            return (ProviderDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    @DeleteMapping("/{id}")
    public Boolean DeleteProvider (@PathVariable("id") int id) {
        try {
            restTemplate.delete("http://userDBManagement/provider/" + id);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
