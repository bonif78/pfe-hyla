package com.hyla.RestExpose.service.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProviderDTO {
    private int idprovider;
    private String name;

    public ProviderDTO(int idprovider, String name) {
        this.idprovider = idprovider;
        this.name = name;
    }

    public ProviderDTO() {
    }

    public int getIdprovider() {
        return idprovider;
    }

    public void setIdprovider(int idprovider) {
        this.idprovider = idprovider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
