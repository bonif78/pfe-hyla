package com.hyla.RestExpose.controler;

import com.hyla.RestExpose.service.DTO.AssetDTO;
import com.hyla.RestExpose.service.DTO.BillingDTO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class AssetControler {
    public List<AssetDTO> GetAssets (int id) throws FileNotFoundException {
        System.out.println("entering get asset");
        List<AssetDTO> assets = new ArrayList<>();

        String filename = "assets" + id + ".csv";
        System.out.println(filename);

        try {
            File file = new File(getClass().getClassLoader().getResource(filename).getFile());

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");
                assets.add(new AssetDTO(values[0], values[1], values[2], Boolean.parseBoolean(values[3]), null));

            }

            return assets;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public BillingDTO getLastBilling(String id) {
        List<BillingDTO> assets = new ArrayList<>();

        String filename =  id + ".csv";

        try {
            File file = new File(getClass().getClassLoader().getResource(filename).getFile());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");
                BillingDTO billingDTO = new BillingDTO(values[0], Float.parseFloat(values[1]));
                assets.add(billingDTO);

            }
            return assets.get(assets.size() - 1);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            return new BillingDTO("01-01-2020",0);
        }
    }
}
