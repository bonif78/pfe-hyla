package com.hyla.RestExpose.service.api;


import com.hyla.RestExpose.service.DTO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserService {


    @Autowired
    RestTemplate restTemplate;

    /**
     * UserLightDTO return only the iduser, the first name and the last name
     * return all the user
     * @return UserLightDTO[]
     */
    @GetMapping
    @ResponseBody
    public UserDTO[] GetAll () {
        try{
            ResponseEntity<UserDTO[]> users = restTemplate.getForEntity("http://userDBManagement/users", UserDTO[].class);
            return users.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * UserDTO contain id first name last name email password role the password is never returned
     * get a user with all his information
     * @param id the id of the ressource to get
     * @return UserDTO
     */
    @GetMapping("/{id}")
    @ResponseBody
    public UserDTO GetUserByID (@PathVariable("id") int id) {
        try{
            return (UserDTO) restTemplate.getForObject("http://userDBManagement/users/" + id, UserDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * get all the teams of one User
     * @param id id of the user
     * @return EquipesDTO[]
     * return all the team (with light information) of one user
     */
    @GetMapping("/{id}/teams")
    @ResponseBody
    public List<EquipesSpecDTO> GetTeamsForUser (@PathVariable("id") int id) throws Exception {
        try{
            ResponseEntity<EquipesHalfDTO[]> equipes = restTemplate.getForEntity("http://userDBManagement/users/" + id + "/equipes", EquipesHalfDTO[].class);
            List<EquipesSpecDTO> result= new ArrayList<>();
            for (EquipesHalfDTO equipe: equipes.getBody()) {
                result.add(new EquipesSpecDTO(equipe));
            }
            return result;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * add a new User
     * @param user the user to add
     * @return UserDTO
     * return the newly added user
     */
    @PostMapping
    @ResponseBody
    public UserDTO InsertUser (@RequestBody UserDTO user) {
        try{

            UserDTO userDTO = (UserDTO) restTemplate.postForObject("http://userDBManagement/users/" , user, UserDTO.class);
            return userDTO;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * add a new User
     * @param id the id of the user who need a reset
     * @return UserDTO
     * return the newly added user
     */
    @PostMapping("/{id}/password")
    @ResponseBody
    public UserDTO InsertUser (@PathVariable("id") int id) {
        try{
            UserDTO userDTO = (UserDTO) restTemplate.postForObject("http://userDBManagement/users/" + id + "/password" , null, UserDTO.class);
            return userDTO;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Login for the user
     * @param user the login information
     * @return LoginDTO
     * the information of the user how logged in
     */
    @PostMapping("/login")
    @ResponseBody
    public LoginDTO Login (@RequestBody LoginDTO user) {
        try{
            return (LoginDTO) restTemplate.postForObject("http://userDBManagement/users/login" , user, LoginDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * modify the user
     * @param user the information to modify
     * @param id of the user to modify
     * @return UserDTO
     * the newly updated user
     */
    @PutMapping("/{id}")
    @ResponseBody
    public UserDTO UpdateUser (@RequestBody UserDTO user, @PathVariable("id") int id) {
        try{
            HttpEntity<UserDTO> roleDTOHttpEntity = new HttpEntity<>(user);
            HttpEntity<UserDTO> result =  restTemplate.exchange("http://userDBManagement/users/" + id, HttpMethod.PUT, roleDTOHttpEntity, UserDTO.class);
            return (UserDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * modify the user
     * @param user the information to modify
     * @param id of the user to modify
     * @return UserDTO
     * the newly updated user
     */
    @PutMapping("/{id}/admin")
    @ResponseBody
    public UserDTO UpdateUserAdmin (@RequestBody UserDTO user, @PathVariable("id") int id) {
        try{
            System.out.println(user);
            System.out.println(user.getRole());
            HttpEntity<UserDTO> roleDTOHttpEntity = new HttpEntity<>(user);
            HttpEntity<UserDTO> result =  restTemplate.exchange("http://userDBManagement/users/" + id + "/admin", HttpMethod.PUT, roleDTOHttpEntity, UserDTO.class);
            return (UserDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * PasswordDTO contain old and new password for the change
     * @param passwordDTO the new and old password
     * @param id of the user who need to change the password
     * @return UserDTO
     * the User with a new password
     */
    @PutMapping("/{id}/password")
    @ResponseBody
    public UserDTO UpdateUserPassword (@RequestBody PasswordDTO passwordDTO, @PathVariable("id") int id) {
        try{
            HttpEntity<PasswordDTO> roleDTOHttpEntity = new HttpEntity<>(passwordDTO);
            HttpEntity<UserDTO> result =  restTemplate.exchange("http://userDBManagement/users/" + id + "/password", HttpMethod.PUT, roleDTOHttpEntity, UserDTO.class);
            return (UserDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    @DeleteMapping("/{id}")
    public Boolean DeleteUser (@PathVariable("id") int id) {
        try {
            restTemplate.delete("http://userDBManagement/users/" + id);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
