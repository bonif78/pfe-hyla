package com.hyla.RestExpose.service.DTO;

import java.util.HashSet;
import java.util.Set;

public class EquipesSpecDTO {
    private int idequipe;
    private String name;

    private UserLightDTO adminClient;

    private Set<ProviderSpecDTO> providerCredentials;

    public EquipesSpecDTO() {
    }

    public EquipesSpecDTO(EquipesHalfDTO equipe) {
        this.idequipe = equipe.getIdequipe();
        this.name = equipe.getName();
        this.adminClient = equipe.getAdminClient();

        this.providerCredentials = new HashSet<ProviderSpecDTO>();
        for (ProviderCredentialsLightDTO pc: equipe.getProviderCredentials()) {
            ProviderSpecDTO tmp = new ProviderSpecDTO(pc);
            this.providerCredentials.add(tmp);
        }
    }

    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserLightDTO getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(UserLightDTO adminClient) {
        this.adminClient = adminClient;
    }

    public Set<ProviderSpecDTO> getProviderCredentials() {
        return providerCredentials;
    }

    public void setProviderCredentials(Set<ProviderSpecDTO> providerCredentials) {
        this.providerCredentials = providerCredentials;
    }
}
