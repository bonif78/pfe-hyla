package com.hyla.RestExpose.service.DTO;

public class RuleDTO {

    private int id;

    private String idAsset;

    private String title;
    private String rule;

    private Boolean isActive;

    public RuleDTO() {
    }

    public RuleDTO(int id, String idAsset, String title, String rule, Boolean isActive) {
        this.id = id;
        this.idAsset = idAsset;

        this.title = title;
        this.rule = rule;

        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getIdAsset() {
        return idAsset;
    }

    public void setIdAsset(String idAsset) {
        this.idAsset = idAsset;
    }
}
