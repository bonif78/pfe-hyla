package com.hyla.RestExpose.service.DTO;


public class UserLightDTO {
    private int idUser;
    private String nameFirst;
    private String nameLast;

    public UserLightDTO(int idUser, String nameFirst, String nameLast) {
        this.idUser = idUser;
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
    }

    public UserLightDTO() {
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public String getNameLast() {
        return nameLast;
    }

    public void setNameLast(String nameLast) {
        this.nameLast = nameLast;
    }
}
