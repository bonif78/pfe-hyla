package com.hyla.RestExpose.service.api;


import com.hyla.RestExpose.service.DTO.AssetDTO;
import com.hyla.RestExpose.service.DTO.ProviderCredentialsDTO;
import com.hyla.RestExpose.service.DTO.ProviderCredentialsLightDTO;
import com.hyla.RestExpose.service.DTO.ProviderSpecDTO;
import com.hyla.RestExpose.service.error.NotFoundExeption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/provider-credentials")
public class ProviderCredentialsService {

    @Autowired
    RestTemplate restTemplate;

    /**
     * providerCredentialLight only id, name and id external
     * @return ProviderCredentialsLightDTO[]
     * reutrn a list of all the provider credential
     */
    @GetMapping
    @ResponseBody
    public ProviderCredentialsLightDTO[] getAll () {
        try{
            ResponseEntity<ProviderCredentialsLightDTO[]> providers = restTemplate.getForEntity("http://userDBManagement/provider-credentials/", ProviderCredentialsLightDTO[].class);
            return providers.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * ProviderCredentailsDTO retur all the information about a providerCredentialDTO
     * @param id of the credential to get
     * @return ProviderCredentialsDTO
     * reuturn the full information of the provider Credential
     */
    @GetMapping("/{id}")
    @ResponseBody
    public ProviderCredentialsDTO GetProviderCredentialByID (@PathVariable("id") int id) {
        try{
            ProviderCredentialsDTO providers = restTemplate.getForObject("http://userDBManagement/provider-credentials/" + id, ProviderCredentialsDTO.class);
            return providers;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * AssetDTO information about one asset
     * @param id of the credential
     * @return List<AssetDTO>
     * @throws FileNotFoundException
     * return all the asset of one account
     */
    @GetMapping("/{id}/assets")
    @ResponseBody
    public List<AssetDTO> GetAssets (@PathVariable("id") int id) throws FileNotFoundException {
        System.out.println("entering get asset");
        List<AssetDTO> assets = new ArrayList<>();

        String filename = "assets" + id + ".csv";
        System.out.println(filename);

        try {
            File file = new File(getClass().getClassLoader().getResource(filename).getFile());

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");
                assets.add(new AssetDTO(values[0], values[1], values[2], Boolean.parseBoolean(values[3]), new Integer(id), values[4]));

            }

            return assets;
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @GetMapping("/{id}/assets/{idAsset}")
    @ResponseBody
    public AssetDTO GetAsset (@PathVariable("id") int id, @PathVariable("idAsset") String idAsset) throws IOException {
        System.out.println("entering get asset");
        List<AssetDTO> assets = new ArrayList<>();

        String filename = "assets" + id + ".csv";
        System.out.println(filename);

        try {
            File file = new File(getClass().getClassLoader().getResource(filename).getFile());

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");
                if(values[0].equals(idAsset)) {
                    return new AssetDTO(values[0], values[1], values[2], Boolean.parseBoolean(values[3]), new Integer(id), values[4]);
                }
            }
            throw new NotFoundExeption(id);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * @param providerCredentials the provider credential to add to the database
     * @return ProviderCredentialsDTO
     * return the newly provider added to the database
     */
    @PostMapping
    @ResponseBody
    public ProviderCredentialsDTO InsertProviderCredential (@RequestBody ProviderCredentialsDTO providerCredentials) {
        try{
            return (ProviderCredentialsDTO) restTemplate.postForObject("http://userDBManagement/provider-credentials/" , providerCredentials, ProviderCredentialsDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * modify a provider credentials
     * @param providerCredentialsDTO the information to change
     * @param id the id of the ressouces to modify
     * @return ProviderCredentialsDTO
     * return the newly modify ressources
     */
    @PutMapping("/{id}")
    @ResponseBody
    public ProviderCredentialsDTO UpdateProviderCredential (@RequestBody ProviderCredentialsDTO providerCredentialsDTO, @PathVariable("id") int id) {
        try{
            HttpEntity<ProviderCredentialsDTO> roleDTOHttpEntity = new HttpEntity<>(providerCredentialsDTO);
            HttpEntity<ProviderCredentialsDTO> result =  restTemplate.exchange("http://userDBManagement/provider-credentials/" + id, HttpMethod.PUT, roleDTOHttpEntity, ProviderCredentialsDTO.class);
            return ((ProviderCredentialsDTO) result.getBody());
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    @DeleteMapping("/{id}")
    public Boolean DeleteProvidercredential (@PathVariable("id") int id) {
        try {
            restTemplate.delete("http://userDBManagement/provider-credentials/" + id);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
