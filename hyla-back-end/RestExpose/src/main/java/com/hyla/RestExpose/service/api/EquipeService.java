package com.hyla.RestExpose.service.api;


import com.hyla.RestExpose.service.DTO.EquipesDTO;
import com.hyla.RestExpose.service.DTO.EquipesFullDTO;
import com.hyla.RestExpose.service.DTO.ProviderCredentialsLightDTO;
import com.hyla.RestExpose.service.DTO.UserLightDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

@RestController
@RequestMapping("/teams")
public class EquipeService {

    @Autowired
    RestTemplate restTemplate;

    /**
     * EquipesDTO just have the name, the adminClient and the id of the team
     * @return EquipesDTO[]
     * a list of all the team
     */
    @GetMapping
    @ResponseBody
    public EquipesFullDTO[] GetAll () {
        try{
            ResponseEntity<EquipesFullDTO[]> equipes = restTemplate.getForEntity("http://userDBManagement/equipes/", EquipesFullDTO[].class);
            return equipes.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * EquipesFullDTO have all the information of the team (name, id, admin client, all the members and all the provider credential)
     * @param id of the team to get
     * @return EquipesFullDTO
     * one team with all informations
     */
    @GetMapping("/{id}")
    @ResponseBody
    public EquipesFullDTO GetEquipeByID (@PathVariable("id") int id) {
        try{
            return (EquipesFullDTO) restTemplate.getForObject("http://userDBManagement/equipes/" + id, EquipesFullDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * providerCrendentialsLightDTO return the id the name and the id for the other database
     * @param id of the team
     * @return ProviderCredentialsLightDTO[]
     * all the provider-credential for one team
     */
    @GetMapping("/{id}/provider-credential")
    @ResponseBody
    public ProviderCredentialsLightDTO[] GetProviderCredentialForEquipe (@PathVariable("id") int id) {
        try{
            ResponseEntity<ProviderCredentialsLightDTO[]> equipes = restTemplate.getForEntity("http://userDBManagement/equipes/" + id + "/provider-credential", ProviderCredentialsLightDTO[].class);
            return equipes.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * EquipesFullDTO all the information of a teams
     * @param equipes the team to add
     * @return EquipesFullDTO
     * the teams add in the database
     */
    @PostMapping
    @ResponseBody
    public EquipesFullDTO InsertEquipe (@RequestBody EquipesFullDTO equipes) {
        try{
            return (EquipesFullDTO) restTemplate.postForObject("http://userDBManagement/equipes/" , equipes, EquipesFullDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * add one user to the team
     * @param user the user to add to the team
     * @param id the id of the team
     * @return EquipesFullDTO
     * the updatated team
     */
    @PostMapping("/{id}/user")
    @ResponseBody
    public EquipesFullDTO InsertEquipeMembre (@RequestBody UserLightDTO user, @PathVariable("id") int id) {
        try{
            return (EquipesFullDTO) restTemplate.postForObject("http://userDBManagement/equipes/" + id + "/user", user, EquipesFullDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * add one provider-credential to the team
     * @param pc the provider to add
     * @param id the team where you add the provider
     * @return EquipesFullDTO
     * the updated team
     */
    @PostMapping("/{id}/credential")
    @ResponseBody
    public EquipesFullDTO InsertEquipeCredential (@RequestBody ProviderCredentialsLightDTO pc, @PathVariable("id") int id) {
        try{
            return (EquipesFullDTO) restTemplate.postForObject("http://userDBManagement/equipes/" + id + "/credential", pc, EquipesFullDTO.class);
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * modify the information of a team
     * @param equipe the information for the modification
     * @param id  the id of a team to modify
     * @return EquipesFullDTO
     * the team successfully modify
     */
    @PutMapping("/{id}")
    @ResponseBody
    public EquipesFullDTO UpdateEquipe (@RequestBody EquipesDTO equipe, @PathVariable("id") int id) {
        try{
            HttpEntity<EquipesDTO> roleDTOHttpEntity = new HttpEntity<>(equipe);
            HttpEntity<EquipesFullDTO> result =  restTemplate.exchange("http://userDBManagement/equipes/" + id, HttpMethod.PUT, roleDTOHttpEntity, EquipesFullDTO.class);
            return (EquipesFullDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * modify the users of a team ==> replace the list by the new list
     * @param equipe the list of  user of the team
     * @param id the id of the team to update
     * @return the newly updated team
     */
    @PutMapping("/{id}/users")
    @ResponseBody
    public EquipesFullDTO UpdateEquipeUsers (@RequestBody Set<UserLightDTO> equipe, @PathVariable("id") int id) {
        try{
            HttpEntity<Set<UserLightDTO>> roleDTOHttpEntity = new HttpEntity<>(equipe);
            HttpEntity<EquipesFullDTO> result =  restTemplate.exchange("http://userDBManagement/equipes/" + id + "/users", HttpMethod.PUT, roleDTOHttpEntity, EquipesFullDTO.class);
            return (EquipesFullDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * modify the provider credential of the team ==> replace the list by the new list
     * @param providerCredentialsLightDTOS the list of provider credential of the team
     * @param id of the team
     * @return EquipesFullDTO
     * the newy updated team
     */
    @PutMapping("/{id}/credentials")
    @ResponseBody
    public EquipesFullDTO UpdateEquipeCredentials (@RequestBody Set<ProviderCredentialsLightDTO> providerCredentialsLightDTOS, @PathVariable("id") int id) {
        try{
            HttpEntity<Set<ProviderCredentialsLightDTO>> roleDTOHttpEntity = new HttpEntity<>(providerCredentialsLightDTOS);
            HttpEntity<EquipesFullDTO> result =  restTemplate.exchange("http://userDBManagement/equipes/" + id + "/credentials", HttpMethod.PUT, roleDTOHttpEntity, EquipesFullDTO.class);
            return (EquipesFullDTO) result.getBody();
        }catch (Exception e) {
            System.out.println("catch");
            e.printStackTrace();
            throw e;
        }
    }

    @DeleteMapping("/{id}")
    public Boolean DeleteEquipe (@PathVariable("id") int id) {
        try {
            restTemplate.delete("http://userDBManagement/equipes/" + id);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
