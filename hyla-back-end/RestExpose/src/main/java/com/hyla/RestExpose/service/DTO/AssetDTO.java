package com.hyla.RestExpose.service.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.hyla.RestExpose.controler.AssetControler;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssetDTO {
    private String assetID;
    private String assetName;
    private String assetOrganisation;
    private Boolean isRunning;
    private BillingDTO lastBilling;
    private Integer idCredential;
    private String description;

    public AssetDTO() {
    }

    public AssetDTO(String assetID, String assetName, String assetOrganisation, Boolean isRunning, Integer credential) {
        AssetControler assetControler = new AssetControler();
        this.assetID = assetID;
        this.assetName = assetName;
        this.assetOrganisation = assetOrganisation;
        this.isRunning = isRunning;
        lastBilling = assetControler.getLastBilling(assetID);
        idCredential = credential;
    }

    public AssetDTO(String assetID, String assetName, String assetOrganisation, Boolean isRunning,  Integer idCredential, String description) {
        AssetControler assetControler = new AssetControler();
        this.assetID = assetID;
        this.assetName = assetName;
        this.assetOrganisation = assetOrganisation;
        this.isRunning = isRunning;
        this.lastBilling = assetControler.getLastBilling(assetID);
        this.idCredential = idCredential;
        this.description = description;
    }

    public String getAssetID() {
        return assetID;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetOrganisation() {
        return assetOrganisation;
    }

    public void setAssetOrganisation(String assetOrganisation) {
        this.assetOrganisation = assetOrganisation;
    }

    public Boolean getRunning() {
        return isRunning;
    }

    public void setRunning(Boolean running) {
        isRunning = running;
    }

    public BillingDTO getLastBilling() {
        return lastBilling;
    }

    public void setLastBilling(BillingDTO lastBilling) {
        this.lastBilling = lastBilling;
    }

    public Integer getIdCredential() {
        return idCredential;
    }

    public void setIdCredential(Integer idCredential) {
        this.idCredential = idCredential;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
