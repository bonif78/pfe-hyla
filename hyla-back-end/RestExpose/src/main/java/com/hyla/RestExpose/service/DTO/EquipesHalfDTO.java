package com.hyla.RestExpose.service.DTO;

import java.util.HashSet;
import java.util.Set;

public class EquipesHalfDTO {
    private int idequipe;
    private String name;

    private UserLightDTO adminClient;

    private Set<ProviderCredentialsLightDTO> providerCredentials;

    public EquipesHalfDTO() {
    }


    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserLightDTO getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(UserLightDTO adminClient) {
        this.adminClient = adminClient;
    }

    public Set<ProviderCredentialsLightDTO> getProviderCredentials() {
        return providerCredentials;
    }

    public void setProviderCredentials(Set<ProviderCredentialsLightDTO> providerCredentials) {
        this.providerCredentials = providerCredentials;
    }


}
