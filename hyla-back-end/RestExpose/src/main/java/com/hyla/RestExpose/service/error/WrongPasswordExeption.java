package com.hyla.RestExpose.service.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WrongPasswordExeption extends RuntimeException {
    public WrongPasswordExeption() {
        super("wrong password");
    }
}
