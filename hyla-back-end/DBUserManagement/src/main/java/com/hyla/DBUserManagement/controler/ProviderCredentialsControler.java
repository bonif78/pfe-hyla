package com.hyla.DBUserManagement.controler;

import com.hyla.DBUserManagement.entity.Provider;
import com.hyla.DBUserManagement.entity.ProviderCredentials;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ProviderCredentialsControler {

    public List<ProviderCredentials> getAll () {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query<ProviderCredentials> query = session.createQuery("select p from provider_credentials p", ProviderCredentials.class);
        return query.getResultList();
    }

    public ProviderCredentials getByID (int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        ProviderCredentials providerCredentials = session.get(ProviderCredentials.class, id);
        if(providerCredentials == null) {
            throw new NotFoundExeption(id);
        }
        return providerCredentials;
    }

    public ProviderCredentials Insert (String username, String email, String Password, String description, String idcredential, int idprovider) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Provider provider = session.get(Provider.class, idprovider);
        if(provider == null){
            throw new NotFoundExeption(idprovider);
        }
        ProviderCredentials providerCredentials = new ProviderCredentials(username, email, Password,description,idcredential,provider);
        session.save(providerCredentials);
        return providerCredentials;
    }

    public ProviderCredentials Update(int id,int idprovider, String username, String email, String description, String idcredential) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        /*Provider provider = session.get(Provider.class, idprovider);
        if(provider == null){
            throw new NotFoundExeption(idprovider);
        } */
        ProviderCredentials update = session.get(ProviderCredentials.class, id);
        if(update == null) {
            throw new NotFoundExeption(id);
        }
        update.setUsername(username);
        update.setEmail(email);
        // update.setDescription(description);
        //update.setProvider(provider);
        update.setIdcredentialext(idcredential);
        return (ProviderCredentials) session.merge(update);
    }

    public void Delete(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        ProviderCredentials providerCredentials = session.get(ProviderCredentials.class,id);
        if(providerCredentials == null) {
            throw new NotFoundExeption(id);
        }
        session.delete(providerCredentials);
    }
}
