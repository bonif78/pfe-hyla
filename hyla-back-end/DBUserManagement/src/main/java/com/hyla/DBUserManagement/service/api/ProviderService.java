package com.hyla.DBUserManagement.service.api;

import com.hyla.DBUserManagement.DTO.ProviderDTO;
import com.hyla.DBUserManagement.controler.ProviderControler;
import com.hyla.DBUserManagement.entity.Provider;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/provider")
public class ProviderService {

    @Autowired
    ProviderControler providerControler;

    @GetMapping
    @ResponseBody
    public List<ProviderDTO> GetAll () {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            List<Provider> provider = this.providerControler.getAll();
            List<ProviderDTO> providerDTOS = new ArrayList<>();
            for (Provider p: provider) {
                providerDTOS.add(new ProviderDTO(p));
            }
            return providerDTOS;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ProviderDTO GetProviderID (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Provider provider = this.providerControler.getByID(id);
            if(provider == null) {
                throw new NotFoundExeption(id);
            }
            return new ProviderDTO(provider);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping
    @ResponseBody
    public ProviderDTO InsertProvider (@RequestBody ProviderDTO provider) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Provider newprovider = this.providerControler.Insert(provider.getName());
            transaction.commit();
            return new ProviderDTO(newprovider);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ProviderDTO UpdateProvider (@RequestBody ProviderDTO providerDTO, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Provider provider = this.providerControler.Update(id,providerDTO.getName());
            transaction.commit();
            return new ProviderDTO(provider);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @DeleteMapping("/{id}")
    public void DeleteProvider (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            this.providerControler.Delete(id);
            transaction.commit();

        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

}
