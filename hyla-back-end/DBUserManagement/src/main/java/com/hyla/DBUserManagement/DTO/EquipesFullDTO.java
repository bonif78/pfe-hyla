package com.hyla.DBUserManagement.DTO;

import com.hyla.DBUserManagement.entity.Equipe;
import com.hyla.DBUserManagement.entity.ProviderCredentials;
import com.hyla.DBUserManagement.entity.User;

import java.util.HashSet;
import java.util.Set;

public class EquipesFullDTO {

    private int idequipe;
    private String name;

    private UserLightDTO adminClient;

    private Set<UserLightDTO> users;

    private Set<ProviderCredentialsLightDTO> providerCredentials;

    public EquipesFullDTO(int idequipe, String name, UserLightDTO adminClient, Set<UserLightDTO> users, Set<ProviderCredentialsLightDTO> providerCredentials) {
        this.idequipe = idequipe;
        this.name = name;
        this.adminClient = adminClient;
        this.users = users;
        this.providerCredentials = providerCredentials;
    }

    public EquipesFullDTO(Equipe equipe) {
        this.idequipe = equipe.getIdequipe();
        this.name = equipe.getName();
        this.adminClient = new UserLightDTO(equipe.getAdminClient());
        this.users = new HashSet<UserLightDTO>();
        for (User u: equipe.getUsers()) {
            this.users.add(new UserLightDTO(u));
        }
        this.providerCredentials = new HashSet<ProviderCredentialsLightDTO>();
        for (ProviderCredentials pc: equipe.getProviderCredentials()) {
            this.providerCredentials.add(new ProviderCredentialsLightDTO(pc));
        }
    }

    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserLightDTO getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(UserLightDTO adminClient) {
        this.adminClient = adminClient;
    }

    public Set<UserLightDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserLightDTO> users) {
        this.users = users;
    }

    public Set<ProviderCredentialsLightDTO> getProviderCredentials() {
        return providerCredentials;
    }

    public void setProviderCredentials(Set<ProviderCredentialsLightDTO> providerCredentials) {
        this.providerCredentials = providerCredentials;
    }
}
