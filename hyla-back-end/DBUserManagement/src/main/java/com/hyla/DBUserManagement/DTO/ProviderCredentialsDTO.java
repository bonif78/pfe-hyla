package com.hyla.DBUserManagement.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.hyla.DBUserManagement.entity.Provider;
import com.hyla.DBUserManagement.entity.ProviderCredentials;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProviderCredentialsDTO {

    private int idprovider_credential;
    private String username;
    private String email;
    private String password;
    private String description;
    private String idcredentialext;


    private ProviderDTO provider;

    public ProviderCredentialsDTO(int idprovider_credential, String username, String email, String password, String description, String idcredentialext, ProviderDTO provider) {
        this.idprovider_credential = idprovider_credential;
        this.username = username;
        this.email = email;
        this.password = password;
        this.description = description;
        this.idcredentialext = idcredentialext;
        this.provider = provider;
    }

    public ProviderCredentialsDTO(ProviderCredentials providerCredentials) {
        this.idprovider_credential = providerCredentials.getIdprovider_credential();
        this.username = providerCredentials.getUsername();
        this.email = providerCredentials.getEmail();
        this.description = providerCredentials.getDescription();
        this.idcredentialext = providerCredentials.getIdcredentialext();
        this.provider = new ProviderDTO(providerCredentials.getProvider());
    }

    public int getIdprovider_credential() {
        return idprovider_credential;
    }

    public void setIdprovider_credential(int idprovider_credential) {
        this.idprovider_credential = idprovider_credential;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdcredentialext() {
        return idcredentialext;
    }

    public void setIdcredentialext(String idcredentialext) {
        this.idcredentialext = idcredentialext;
    }

    public ProviderDTO getProvider() {
        return provider;
    }

    public void setProvider(ProviderDTO provider) {
        this.provider = provider;
    }
}
