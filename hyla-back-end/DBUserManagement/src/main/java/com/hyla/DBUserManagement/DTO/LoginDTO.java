package com.hyla.DBUserManagement.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginDTO {

    private String email;
    private String password;
    private int id;
    private String nameFirst;
    private String nameLast;

    public LoginDTO() {
    }

    public LoginDTO(String email, String password, int id) {
        this.email = email;
        this.password = password;
        this.id = id;
    }

    public LoginDTO(String email, String password, int id, String nameFirst, String nameLast) {
        this.email = email;
        this.password = password;
        this.id = id;
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public String getNameLast() {
        return nameLast;
    }

    public void setNameLast(String nameLast) {
        this.nameLast = nameLast;
    }
}
