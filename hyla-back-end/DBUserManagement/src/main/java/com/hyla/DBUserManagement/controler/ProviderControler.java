package com.hyla.DBUserManagement.controler;

import com.hyla.DBUserManagement.entity.Provider;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ProviderControler {

    public List<Provider> getAll () {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query<Provider> query = session.createQuery("select p from provider p", Provider.class);
        return query.getResultList();
    }

    public Provider getByID (int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        return session.get(Provider.class, id);
    }

    public Provider Insert (String name) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Provider provider = new Provider(name);
        session.save(provider);
        return provider;
    }

    public Provider Update(int id, String name) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Provider update = session.get(Provider.class, id);
        if(update==null)
            throw new NotFoundExeption(id);
        update.setName(name);
        return (Provider) session.merge(update);
    }

    public void Delete(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Provider provider = session.get(Provider.class,id);
        session.delete(provider);
    }
}
