package com.hyla.DBUserManagement.service.api;

import com.hyla.DBUserManagement.DTO.ProviderCredentialsDTO;
import com.hyla.DBUserManagement.DTO.ProviderCredentialsLightDTO;
import com.hyla.DBUserManagement.controler.ProviderControler;
import com.hyla.DBUserManagement.controler.ProviderCredentialsControler;
import com.hyla.DBUserManagement.entity.ProviderCredentials;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/provider-credentials")
public class ProviderCredentialsService {

    @Autowired
    ProviderCredentialsControler providerCredentialsControler;

    @GetMapping
    @ResponseBody
    public List<ProviderCredentialsLightDTO> getAll () {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            List<ProviderCredentials> providerCredentials = this.providerCredentialsControler.getAll();
            List<ProviderCredentialsLightDTO> providerCredentialsLightDTOS = new ArrayList<>();
            for (ProviderCredentials pc: providerCredentials) {
                providerCredentialsLightDTOS.add(new ProviderCredentialsLightDTO(pc));
            }
            return providerCredentialsLightDTOS;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ProviderCredentialsDTO GetProviderCredentialByID (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            ProviderCredentials providerCredentials = this.providerCredentialsControler.getByID(id);
            return new ProviderCredentialsDTO(providerCredentials);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping
    @ResponseBody
    public ProviderCredentialsDTO InsertProviderCredential (@RequestBody ProviderCredentialsDTO providerCredentials) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            ProviderCredentials NewProviderCredentials = this.providerCredentialsControler.Insert(providerCredentials.getUsername(), providerCredentials.getEmail(), providerCredentials.getPassword(), providerCredentials.getDescription(),providerCredentials.getIdcredentialext(),providerCredentials.getProvider().getIdprovider());
            transaction.commit();
            return new ProviderCredentialsDTO(NewProviderCredentials);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ProviderCredentialsDTO UpdateProviderCredential (@RequestBody ProviderCredentialsDTO providerCredentialsDTO, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            ProviderCredentials providerCredentials = this.providerCredentialsControler.Update(id,providerCredentialsDTO.getProvider().getIdprovider(),providerCredentialsDTO.getUsername(),providerCredentialsDTO.getEmail(),providerCredentialsDTO.getDescription(),providerCredentialsDTO.getIdcredentialext());
            transaction.commit();
            return new ProviderCredentialsDTO(providerCredentials);

        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @DeleteMapping("/{id}")
    public void DeleteProvidercredential (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            this.providerCredentialsControler.Delete(id);
            transaction.commit();

        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

}
