package com.hyla.DBUserManagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity(name="provider_credentials")
@Table(name="provider_credentials", schema = "public")
public class ProviderCredentials {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int idprovider_credential;
    String username;
    String email;
    String password;
    String description;
    String idcredentialext;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idprovider")
    private Provider provider;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="provider_equipe", joinColumns = {@JoinColumn(name="id_provider_credentials")}, inverseJoinColumns = {@JoinColumn(name="id_equipe")})
    private Set<Equipe> equipes;

    public ProviderCredentials() {
    }

    public ProviderCredentials(String username, String email, String password, String description, String idcredentialext, Provider provider) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.description = description;
        this.idcredentialext = idcredentialext;
        this.provider = provider;
    }

    public int getIdprovider_credential() {
        return idprovider_credential;
    }

    public void setIdprovider_credential(int idprovider_credential) {
        this.idprovider_credential = idprovider_credential;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdcredentialext() {
        return idcredentialext;
    }

    public void setIdcredentialext(String idcredentialext) {
        this.idcredentialext = idcredentialext;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Set<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(Set<Equipe> equipes) {
        this.equipes = equipes;
    }

    @Override
    public String toString() {
        return "ProviderCredentials{" +
                "idprovider_credential=" + idprovider_credential +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", description='" + description + '\'' +
                ", idcredentialext='" + idcredentialext + '\'' +
                ", provider=" + provider +
                '}';
    }
}

