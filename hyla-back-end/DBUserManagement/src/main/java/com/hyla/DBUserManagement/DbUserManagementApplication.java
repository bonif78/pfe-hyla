package com.hyla.DBUserManagement;

import com.hyla.DBUserManagement.controler.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DbUserManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbUserManagementApplication.class, args);
	}

	@Bean
	public EquipeControler getEquipeControler() {
		return new EquipeControler();
	}

	@Bean
	public ProviderControler getProviderControler() {
		return new ProviderControler();
	}

	@Bean
	public ProviderCredentialsControler getProviderCredentialsControler() {
		return new ProviderCredentialsControler();
	}

	@Bean
	public RoleControler getRoleControler() {
		return new RoleControler();
	}

	@Bean
	public UserControler getRoleCntroler() {
		return new UserControler();
	}

}
