package com.hyla.DBUserManagement.entity;

import javax.persistence.*;

@Entity(name="role")
@Table(name="role", schema = "public")
public class Role {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int idrole;
    private String name;
    private int privilegeLevel;

    public Role() {
    }

    public Role(String name, int privilegeLevel) {
        this.name = name;
        this.privilegeLevel = privilegeLevel;
    }

    public int getIdrole() {
        return idrole;
    }

    public void setIdrole(int idrole) {
        this.idrole = idrole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrivilegeLevel() {
        return privilegeLevel;
    }

    public void setPrivilegeLevel(int privilegeLevel) {
        this.privilegeLevel = privilegeLevel;
    }

    @Override
    public String toString() {
        return "Role{" +
                "idrole=" + idrole +
                ", name='" + name + '\'' +
                ", privilegeLevel=" + privilegeLevel +
                '}';
    }
}
