package com.hyla.DBUserManagement.controler;

import com.hyla.DBUserManagement.entity.Role;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class RoleControler {

    public List<Role> getAll () {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("select r from role r", Role.class);
        return query.getResultList();
    }

    public Role getByID (int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        return session.get(Role.class, id);
    }

    public Role Insert (String name,  int level) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Role role = new Role(name,level);
        session.save(role);
        return role;
    }

    public Role Update(int id, String name,  int level) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Role update = session.get(Role.class, id);
        if(update == null) {
            throw new NotFoundExeption(id);
        }
        update.setName(name);
        update.setPrivilegeLevel(level);
        return (Role) session.merge(update);
    }

    public void Delete(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Role role = session.get(Role.class,id);
        if(role == null) {
            throw new NotFoundExeption(id);
        }
            session.delete(role);
    }
}
