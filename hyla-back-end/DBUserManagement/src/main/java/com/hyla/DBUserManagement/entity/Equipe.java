package com.hyla.DBUserManagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "equipe")
@Table(name="equipe", schema = "public")
public class Equipe {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int idequipe;
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="adminclient")
    private User adminClient;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="userequipe", joinColumns = {@JoinColumn(name="idequipe")}, inverseJoinColumns = {@JoinColumn(name="iduser")})
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="provider_equipe", joinColumns = {@JoinColumn(name="id_equipe")}, inverseJoinColumns = {@JoinColumn(name="id_provider_credentials")})
    private Set<ProviderCredentials> providerCredentials;

    public Equipe() {
    }

    public Equipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public Equipe(String nom, User adminClient) {
        this.name = nom;
        this.adminClient = adminClient;
    }

    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String nom) {
        this.name = nom;
    }

    public User getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(User adminClient) {
        this.adminClient = adminClient;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<ProviderCredentials> getProviderCredentials() {
        return providerCredentials;
    }

    public void setProviderCredentials(Set<ProviderCredentials> providerCredentials) {
        this.providerCredentials = providerCredentials;
    }

    @Override
    public String toString() {
        return "Equipe{" +
                "idequipe=" + idequipe +
                ", nom='" + name + '\'' +
                ", adminClient=" + adminClient +
                '}';
    }
}
