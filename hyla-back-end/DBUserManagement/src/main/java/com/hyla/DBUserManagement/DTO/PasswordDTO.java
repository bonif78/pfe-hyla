package com.hyla.DBUserManagement.DTO;

public class PasswordDTO {
    private String oldPassword;
    private String newPassword;
    private Boolean success;

    public PasswordDTO() {
    }

    public PasswordDTO(String oldPassword, String newPassword, Boolean success) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.success = success;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
