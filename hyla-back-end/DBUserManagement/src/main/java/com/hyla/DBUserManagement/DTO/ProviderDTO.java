package com.hyla.DBUserManagement.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.hyla.DBUserManagement.entity.Provider;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProviderDTO {
    private int idprovider;
    private String name;

    public ProviderDTO(int idprovider, String name) {
        this.idprovider = idprovider;
        this.name = name;
    }

    public ProviderDTO(Provider provider) {
        this.idprovider = provider.getIdprovider();
        this.name = provider.getName();
    }

    public int getIdprovider() {
        return idprovider;
    }

    public void setIdprovider(int idprovider) {
        this.idprovider = idprovider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
