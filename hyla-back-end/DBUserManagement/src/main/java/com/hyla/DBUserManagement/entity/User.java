package com.hyla.DBUserManagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity(name="user")
@Table(name="user", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name = "iduser", updatable = false, nullable = false)
    private int idUser;
    private String nameFirst;
    private String nameLast;
    private String email;
    private String Password;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idrole")
    private Role role;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="userequipe", joinColumns = {@JoinColumn(name="iduser")}, inverseJoinColumns = {@JoinColumn(name="idequipe")})
    private Set<Equipe> equipes;

    public User() {
    }



    public User(String nameFirst, String nameLast, String email, String password, Role idrole) {
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
        this.email = email;
        Password = password;
        this.role = idrole;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public String getNameLast() {
        return nameLast;
    }

    public void setNameLast(String nameLast) {
        this.nameLast = nameLast;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public Set<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(Set<Equipe> equipes) {
        this.equipes = equipes;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idUser +
                ", nameFirst='" + nameFirst + '\'' +
                ", nameLast='" + nameLast + '\'' +
                ", email='" + email + '\'' +
                ", Password='" + Password + '\'' +
                ", role='" + this.role + '\'' +
                '}';
    }
}

