package com.hyla.DBUserManagement.DTO;

import com.hyla.DBUserManagement.entity.Equipe;
import com.hyla.DBUserManagement.entity.ProviderCredentials;
import java.util.HashSet;
import java.util.Set;

public class EquipesHalfDTO {
    private int idequipe;
    private String name;


    private UserLightDTO adminClient;

    private Set<ProviderCredentialsLightDTO> providerCredentials;

    public EquipesHalfDTO() {
    }

    public EquipesHalfDTO(Equipe equipe) {
        this.idequipe = equipe.getIdequipe();
        this.name = equipe.getName();
        this.adminClient = new UserLightDTO(equipe.getAdminClient());

        this.providerCredentials = new HashSet<ProviderCredentialsLightDTO>();
        for (ProviderCredentials pc: equipe.getProviderCredentials()) {
            this.providerCredentials.add(new ProviderCredentialsLightDTO(pc));
        }
    }

    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserLightDTO getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(UserLightDTO adminClient) {
        this.adminClient = adminClient;
    }

    public Set<ProviderCredentialsLightDTO> getProviderCredentials() {
        return providerCredentials;
    }

    public void setProviderCredentials(Set<ProviderCredentialsLightDTO> providerCredentials) {
        this.providerCredentials = providerCredentials;
    }
}
