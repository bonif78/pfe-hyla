package com.hyla.DBUserManagement.service.api;

import com.hyla.DBUserManagement.DTO.EquipesDTO;
import com.hyla.DBUserManagement.DTO.EquipesFullDTO;
import com.hyla.DBUserManagement.DTO.ProviderCredentialsLightDTO;
import com.hyla.DBUserManagement.DTO.UserLightDTO;
import com.hyla.DBUserManagement.controler.EquipeControler;
import com.hyla.DBUserManagement.entity.Equipe;
import com.hyla.DBUserManagement.entity.ProviderCredentials;
import com.hyla.DBUserManagement.entity.User;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/equipes")
public class EquipeService {

    @Autowired
    EquipeControler equipeControler;

    @GetMapping
    @ResponseBody
    public List<EquipesFullDTO> GetAll () {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            List<Equipe> equipe = this.equipeControler.getAll();
            List<EquipesFullDTO> equipesDTOS = new ArrayList<>();
            for (Equipe e: equipe) {
                equipesDTOS.add(new EquipesFullDTO(e));
            }
            return equipesDTOS;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public EquipesFullDTO GetEquipeByID (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Equipe equipe = this.equipeControler.getByID(id);
            if(equipe == null){
                throw new NotFoundExeption(id);
            }
            return new EquipesFullDTO(equipe);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @GetMapping("/{id}/provider-credential")
    @ResponseBody
    public Set<ProviderCredentialsLightDTO> GetProviderCredentialForEquipe (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Set<ProviderCredentials> providerCredentials = this.equipeControler.getProviderForTeam(id);
            Set<ProviderCredentialsLightDTO> providerCredentialsLightDTOS = new HashSet<>();
            for(ProviderCredentials pc : providerCredentials) {
                providerCredentialsLightDTOS.add(new ProviderCredentialsLightDTO(pc));
            }
            return providerCredentialsLightDTOS;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping
    @ResponseBody
    public EquipesFullDTO InsertEquipe (@RequestBody EquipesFullDTO equipes) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Equipe equipe = this.equipeControler.Insert(equipes.getName(),equipes.getAdminClient().getIdUser(),equipes.getUsers(),equipes.getProviderCredentials());
            transaction.commit();
            return new EquipesFullDTO(equipe);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping("/{id}/user")
    @ResponseBody
    public EquipesFullDTO InsertEquipeUser (@RequestBody UserLightDTO user, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Equipe equipe = this.equipeControler.AddMember(id, user.getIdUser());
            transaction.commit();
            return new EquipesFullDTO(equipe);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping("/{id}/credential")
    @ResponseBody
    public EquipesFullDTO InsertEquipeCredential (@RequestBody ProviderCredentialsLightDTO providerCredentialsLightDTO, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Equipe equipe = this.equipeControler.AddCredentials(id, providerCredentialsLightDTO.getIdprovider_credential());
            transaction.commit();
            return new EquipesFullDTO(equipe);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public EquipesFullDTO UpdateEquipe (@RequestBody EquipesDTO equipe, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Equipe newEquipe = this.equipeControler.Update(id,equipe.getName(), equipe.getAdminClient().getIdUser());
            transaction.commit();
            return new EquipesFullDTO(newEquipe);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}/users")
    @ResponseBody
    public EquipesFullDTO UpdateEquipeUsers (@RequestBody Set<UserLightDTO> users, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Equipe newEquipe = this.equipeControler.UpdateUsers(id,users);
            transaction.commit();
            return new EquipesFullDTO(newEquipe);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}/credentials")
    @ResponseBody
    public EquipesFullDTO UpdateEquipeCredentials (@RequestBody Set<ProviderCredentialsLightDTO> pc, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Equipe newEquipe = this.equipeControler.UpdateCredentials(id,pc);
            transaction.commit();
            return new EquipesFullDTO(newEquipe);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @DeleteMapping("/{id}")
    public void DeleteEquipe (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            this.equipeControler.Delete(id);
            transaction.commit();

        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    public EquipeControler getEquipeControler() {
        return equipeControler;
    }

    public void setEquipeControler(EquipeControler equipeControler) {
        this.equipeControler = equipeControler;
    }
}
