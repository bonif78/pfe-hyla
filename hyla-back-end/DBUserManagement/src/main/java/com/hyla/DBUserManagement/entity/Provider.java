package com.hyla.DBUserManagement.entity;

import javax.persistence.*;

@Entity(name="provider")
@Table(name="provider", schema = "public")
public class Provider {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int idprovider;
    private String name;

    public Provider() {
    }

    public Provider(String name) {
        this.name = name;
    }

    public int getIdprovider() {
        return idprovider;
    }

    public void setIdprovider(int idprovider) {
        this.idprovider = idprovider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "idprovider=" + idprovider +
                ", name='" + name + '\'' +
                '}';
    }
}
