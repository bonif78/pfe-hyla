package com.hyla.DBUserManagement.controler;

import com.hyla.DBUserManagement.entity.Equipe;
import com.hyla.DBUserManagement.entity.Role;
import com.hyla.DBUserManagement.entity.User;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.service.ErrorControler.WrongPasswordExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Set;

public class UserControler {

    public List<User> getAll() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query<User> userQuery = session.createQuery("select  u from user u", User.class );
        return userQuery.getResultList();
    }

    public User getByID (int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        return session.get(User.class, id);
    }

    public Set<Equipe> getEquipeForUser(int id) {
        User user = this.getByID(id);
        System.out.println(user);
        if(user == null) {
            throw new NotFoundExeption(id);
        }
        Hibernate.initialize(user.getEquipes());
        return user.getEquipes();
    }

    public User Insert (String LastName, String FirstName, String email, String Password, int idrole) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        User user = new User(FirstName, LastName, email, Password, session.get(Role.class,idrole));
        session.save(user);
        return user;
    }

    public User Update(int id, String LastName, String FirstName, String email) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        User user = session.get(User.class, id);
        if(user == null){
            throw new NotFoundExeption(id);
        }
        user.setNameLast(LastName);
        user.setNameFirst(FirstName);
        user.setEmail(email);
        return (User) session.merge(user);
    }

    public User UpdateAdmin(int id, String nameFirst, String nameLast, String email, int IdRole) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        User user = session.get(User.class, id);
        System.out.println(user);
        if(user == null){
            throw new NotFoundExeption(id);
        }
        user.setRole(session.get(Role.class, IdRole));
        user.setNameFirst(nameFirst);
        user.setNameLast(nameLast);
        user.setEmail(email);
        return (User) session.merge(user);
    }

    public User UpdatePassword (int id, String oldPassword, String newPassword) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        User user = session.get(User.class, id);
        if(user == null) {
            throw new NotFoundExeption(id);
        }
        if(user.getPassword().equals(oldPassword)) {
            user.setPassword(newPassword);
            return (User) session.merge(user);
        } else {
            throw new WrongPasswordExeption();
        }
    }

    public User ResetPassword (int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        User user = session.get(User.class, id);
        if (user == null) {
            throw new NotFoundExeption(id);
        }
        user.setPassword("pv8hV{9M$56`Nt");
        return (User) session.merge(user);
    }

    public void Delete(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        User user = session.get(User.class,id);
        if(user == null) {
            throw new NotFoundExeption(id);
        }
        session.delete(user);
    }
}