package com.hyla.DBUserManagement.controler;

import com.hyla.DBUserManagement.DTO.ProviderCredentialsLightDTO;
import com.hyla.DBUserManagement.DTO.UserLightDTO;
import com.hyla.DBUserManagement.entity.Equipe;
import com.hyla.DBUserManagement.entity.ProviderCredentials;
import com.hyla.DBUserManagement.entity.User;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EquipeControler {

    public List<Equipe> getAll() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Query<Equipe> equipeQuery = session.createQuery("select e from equipe e", Equipe.class);
        return equipeQuery.getResultList();
    }

    public Equipe getByID (int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Equipe equipe =  session.get(Equipe.class, id);
        if(equipe == null) {
            throw new NotFoundExeption(id);
        }
        return equipe;
    }

    public Set<ProviderCredentials> getProviderForTeam (int id){
        Equipe equipe = getByID(id);
        if(equipe == null) {
            throw new NotFoundExeption(id);
        }
        return equipe.getProviderCredentials();
    }

    public Equipe Insert (String name, int adminclient, Set<UserLightDTO> users, Set<ProviderCredentialsLightDTO> providerCredentialsLightDTOS) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        User adminClient = session.get(User.class,adminclient);
        if(adminClient == null) {
            throw new NotFoundExeption(adminclient);
        }
        Set<User> userSet = new HashSet<>();
        userSet.add(adminClient);
        for( UserLightDTO u: users) {
            User nexUser = session.get(User.class, u.getIdUser());
            if(nexUser != null) {
                userSet.add(nexUser);
            }
        }
        Set<ProviderCredentials> providerCredentials = new HashSet<>();
        for(ProviderCredentialsLightDTO pc : providerCredentialsLightDTOS) {
            ProviderCredentials p = session.get(ProviderCredentials.class, pc.getIdprovider_credential());
            if(p != null) {
                providerCredentials.add(p);
            }
        }
        Equipe equipe = new Equipe(name,adminClient);
        equipe.setProviderCredentials(providerCredentials);
        equipe.setUsers(userSet);
        session.save(equipe);
        return equipe;
    }

    public Equipe AddMember (int id, int idUser) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Equipe update = session.get(Equipe.class, id);
        User user = session.get(User.class, idUser);
        if(update == null || user == null) {
            throw new NotFoundExeption(id);
        }
        update.getUsers().add(user);

        return (Equipe) session.merge(update);
    }

    public Equipe AddCredentials (int id, int idCredential) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Equipe update = session.get(Equipe.class, id);
        ProviderCredentials providerCredentials = session.get(ProviderCredentials.class, idCredential);
        if(update == null){
            throw new NotFoundExeption(id);
        } else if(providerCredentials == null) {
            throw new NotFoundExeption(idCredential);
        }
        update.getProviderCredentials().add(providerCredentials);

        return (Equipe) session.merge(update);
    }

    public Equipe Update(int id, String name,  int adminclient) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Equipe update = session.get(Equipe.class, id);
        if(update == null) {
            throw new NotFoundExeption(id);
        }

        User adminClient = session.get(User.class,adminclient);
        if(adminClient == null) {
            throw new NotFoundExeption(adminclient);
        }
        update.setName(name);
        update.setAdminClient(adminClient);
        update.getUsers().add(adminClient);
        return (Equipe) session.merge(update);
    }

    public Equipe UpdateUsers(int id, Set<UserLightDTO> users) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Equipe update = session.get(Equipe.class, id);
        Set<User> userSet = new HashSet<>();
        if(update.getAdminClient() != null) {
            userSet.add(update.getAdminClient());
        }
        for (User u : update.getUsers()) {
            userSet.add(u);
        }
        for (UserLightDTO u: users) {
            User tmp = session.get(User.class, u.getIdUser());
            userSet.add(tmp);
        }
        update.setUsers(userSet);
        return (Equipe) session.merge(update);
    }

    public Equipe UpdateCredentials(int id, Set<ProviderCredentialsLightDTO> providerCredentialsLightDTOS) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Equipe update = session.get(Equipe.class, id);
        Set<ProviderCredentials> providerCredentials = new HashSet<>();
        for (ProviderCredentialsLightDTO pc: providerCredentialsLightDTOS) {
            ProviderCredentials tmp = session.get(ProviderCredentials.class, pc.getIdprovider_credential());
            providerCredentials.add(tmp);
        }
        update.setProviderCredentials(providerCredentials);
        return (Equipe) session.merge(update);
    }


    public void Delete(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Equipe equipe = session.get(Equipe.class,id);
        if(equipe == null) {
            throw new NotFoundExeption(id);
        }
        session.delete(equipe);
    }
}
