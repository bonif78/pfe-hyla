package com.hyla.DBUserManagement.DTO;

import com.hyla.DBUserManagement.entity.ProviderCredentials;

public class ProviderCredentialsLightDTO {
    private int idprovider_credential;
    private String username;
    private String email;

    private String idcredentialext;

    public ProviderCredentialsLightDTO(int idprovider_credential, String username, String idcredentialext) {
        this.idprovider_credential = idprovider_credential;
        this.username = username;
        this.idcredentialext = idcredentialext;
    }

    public ProviderCredentialsLightDTO(ProviderCredentials providerCredentials) {
        this.idprovider_credential = providerCredentials.getIdprovider_credential();
        this.username = providerCredentials.getUsername();
        this.idcredentialext = providerCredentials.getIdcredentialext();
        this.email = providerCredentials.getEmail();
    }

    public int getIdprovider_credential() {
        return idprovider_credential;
    }

    public void setIdprovider_credential(int idprovider_credential) {
        this.idprovider_credential = idprovider_credential;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdcredentialext() {
        return idcredentialext;
    }

    public void setIdcredentialext(String idcredentialext) {
        this.idcredentialext = idcredentialext;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
