package com.hyla.DBUserManagement.service.api;

import com.hyla.DBUserManagement.DTO.*;
import com.hyla.DBUserManagement.controler.UserControler;
import com.hyla.DBUserManagement.entity.Equipe;
import com.hyla.DBUserManagement.entity.User;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserService {

    @Autowired
    private UserControler userControler;


    @GetMapping
    @ResponseBody
    public List<UserDTO> GetAllUser () {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            List<User> users = this.userControler.getAll();
            List<UserDTO> userDTOS = new ArrayList<>();
            for(User u : users) {
                userDTOS.add(new UserDTO(u));
            }
            return userDTOS;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public UserDTO GetUserByID (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            User user = this.userControler.getByID(id);
            if(user == null) {
                throw new NotFoundExeption(id);
            }
            return new UserDTO(user);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @GetMapping("/{id}/equipes")
    @ResponseBody
    public Set<EquipesHalfDTO> GetTeamsForUser (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Set<Equipe> equipes = this.userControler.getEquipeForUser(id);
            Set<EquipesHalfDTO> equipesDTOS = new HashSet<>();
            for (Equipe e: equipes) {
                equipesDTOS.add(new EquipesHalfDTO(e));
            }
            return equipesDTOS;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping
    @ResponseBody
    public UserDTO InsertUser (@RequestBody UserDTO user) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            User newUser = this.userControler.Insert(user.getNameLast(),user.getNameFirst(),user.getEmail(), user.getPassword(), user.getRole().getIdrole());
            transaction.commit();
            return new UserDTO(newUser);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping("/{id}/password")
    @ResponseBody
    public UserDTO InsertUser (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            User newUser = this.userControler.ResetPassword(id);
            transaction.commit();
            return new UserDTO(newUser);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping("/login")
    @ResponseBody
    public LoginDTO login (@RequestBody LoginDTO user) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query<User> query = session.createQuery("select  u from user u where u.email = :email", User.class);
            query.setParameter("email", user.getEmail());
            User user1 = query.getSingleResult();
            if(user1 == null) {
                throw new NotFoundExeption(0);
            }
            if(user1.getPassword().equals(user.getPassword())){
             return new LoginDTO(user1.getEmail(), null, user1.getIdUser(), user1.getNameFirst(), user1.getNameLast());
            }
            else {
                throw new NotFoundExeption(0);
            }
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public UserDTO UpdateUser (@RequestBody UserDTO user, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            User NewUser = this.userControler.Update(id,user.getNameLast(),user.getNameFirst(),user.getEmail());
            transaction.commit();
            return new UserDTO(NewUser);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}/admin")
    @ResponseBody
    public UserDTO UpdateUserAdmin (@RequestBody UserDTO user, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            User NewUser = this.userControler.UpdateAdmin(id,user.getNameFirst(), user.getNameLast(), user.getEmail(), user.getRole().getIdrole());
            transaction.commit();
            return new UserDTO(NewUser);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}/password")
    @ResponseBody
    public UserDTO UpdateUserPassword (@RequestBody PasswordDTO passwordDTO, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            User NewUser = this.userControler.UpdatePassword(id, passwordDTO.getOldPassword(), passwordDTO.getNewPassword());
            transaction.commit();
            return new UserDTO(NewUser);
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }


    @DeleteMapping("/{id}")
    public void DeleteUser (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            this.userControler.Delete(id);
            transaction.commit();

        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;

        } finally {
            if(session != null) {
                session.close();
            }
        }
    }
}
