package com.hyla.DBUserManagement.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.hyla.DBUserManagement.entity.User;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    private int idUser;
    private String nameFirst;
    private String nameLast;
    private String email;
    private String Password;

    private RoleDTO role;

    public UserDTO(int idUser, String nameFirst, String nameLast, String email, String password, RoleDTO role) {
        this.idUser = idUser;
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
        this.email = email;
        Password = password;
        this.role = role;
    }

    public UserDTO(User user) {
        this.idUser = user.getIdUser();
        this.nameFirst = user.getNameFirst();
        this.nameLast = user.getNameLast();
        this.email = user.getEmail();

        this.role = new RoleDTO(user.getRole());
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public String getNameLast() {
        return nameLast;
    }

    public void setNameLast(String nameLast) {
        this.nameLast = nameLast;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }
}
