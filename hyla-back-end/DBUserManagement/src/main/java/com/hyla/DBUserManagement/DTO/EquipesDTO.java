package com.hyla.DBUserManagement.DTO;

import com.hyla.DBUserManagement.entity.Equipe;

public class EquipesDTO {
    private int idequipe;
    private String name;

    private UserDTO adminClient;



    public EquipesDTO(int idequipe, String name, UserDTO adminClient) {
        this.idequipe = idequipe;
        this.name = name;
        this.adminClient = adminClient;
    }

    public EquipesDTO(Equipe equipe) {
        this.idequipe = equipe.getIdequipe();
        this.name = equipe.getName();
        this.adminClient = new UserDTO(equipe.getAdminClient());
    }

    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getAdminClient() {
        return adminClient;
    }

    public void setAdminClient(UserDTO adminClient) {
        this.adminClient = adminClient;
    }
}
