package com.hyla.DBUserManagement.service.api;

import com.hyla.DBUserManagement.DTO.RoleDTO;
import com.hyla.DBUserManagement.controler.RoleControler;
import com.hyla.DBUserManagement.entity.Role;
import com.hyla.DBUserManagement.service.ErrorControler.NotFoundExeption;
import com.hyla.DBUserManagement.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/roles")
public class RoleService {
    @Autowired
    RoleControler roleControler;

    @GetMapping
    @ResponseBody
    public List<RoleDTO> GetRoleByID () {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            List<Role> roles = this.roleControler.getAll();
            List<RoleDTO> roleDTOS = new ArrayList<>();
            for (Role r: roles) {
                roleDTOS.add(new RoleDTO(r));
            }
            return roleDTOS;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    public RoleDTO GetRoleByID (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        System.out.println(id);
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Role role = this.roleControler.getByID(id);
            if(role == null) {
                throw new NotFoundExeption(id);
            }
            return new RoleDTO(role.getIdrole(), role.getName(), role.getPrivilegeLevel());
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PostMapping
    @ResponseBody
    public RoleDTO InsertRole (@RequestBody RoleDTO role) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Role newRole = this.roleControler.Insert(role.getName(), role.getPrivilegeLevel());
            transaction.commit();
            return new RoleDTO(newRole.getIdrole(), newRole.getName(), newRole.getPrivilegeLevel());
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @PutMapping("/{id}")
    @ResponseBody
    public RoleDTO UpdateRole (@RequestBody RoleDTO roleDTO, @PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Role role = this.roleControler.Update(id,roleDTO.getName(), roleDTO.getPrivilegeLevel());
            transaction.commit();
            return new RoleDTO(role.getIdrole(),role.getName(),role.getPrivilegeLevel());

        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @DeleteMapping("/{id}")
    public boolean DeleteRole (@PathVariable("id") int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            this.roleControler.Delete(id);
            transaction.commit();
            return true;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    public RoleControler getRoleControler() {
        return roleControler;
    }

    public void setRoleControler(RoleControler roleControler) {
        this.roleControler = roleControler;
    }
}
