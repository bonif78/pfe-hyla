package com.hyla.DBUserManagement.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.hyla.DBUserManagement.entity.Role;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleDTO {
    private int idrole;
    private String name;
    private int privilegeLevel;

    public RoleDTO(int idrole, String name, int privilegeLevel) {
        this.idrole = idrole;
        this.name = name;
        this.privilegeLevel = privilegeLevel;
    }

    public RoleDTO(Role role) {
        this.idrole = role.getIdrole();
        this.name = role.getName();
        this.privilegeLevel = role.getPrivilegeLevel();
    }

    public int getIdrole() {
        return idrole;
    }

    public void setIdrole(int idrole) {
        this.idrole = idrole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrivilegeLevel() {
        return privilegeLevel;
    }

    public void setPrivilegeLevel(int privilegeLevel) {
        this.privilegeLevel = privilegeLevel;
    }
}
