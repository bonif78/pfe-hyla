export default function (ctx) {
  // We consider that the params are, by default, valid
  let hasValidParams = true

  // If the route goes by an account
  if (ctx.params.idAccount !== undefined) {
    // We check if the param is valid
    hasValidParams = !isNaN(ctx.params.idAccount)

    // If it is, and the route goes by  a providerCredential
    if (ctx.params.idProviderCredentials !== undefined) {
      // We check if the param is valid
      hasValidParams = !isNaN(ctx.params.idProviderCredentials)

      // If it is, and the route goes by an asset..
      // Well, the id for an asset is always valid :)
    }
  }

  // If any parameter is invalid : REDIRECTION
  if (!hasValidParams) {
    // Is the user logged ?
    const isLogged = ctx.store.getters['authentification/isAuthenticated']

    // If logged in : goes to /compte-client
    // Otherwise : to /utilisateur/login
    if (isLogged) {
      // We get the user's id
      const iduser = ctx.store.getters['authentification/idUser']
      ctx.redirect(301, '/account/' + iduser + '/tableau-de-bord')
    } else {
      ctx.redirect(301, '/utilisateur/login')
    }
  }
}
