export default function (ctx) {
  // Is the user logged ?
  const isLogged = ctx.store.getters['authentification/isAuthenticated']

  // Is this the user's first connexion ?
  const isFirstConnexion = ctx.store.getters['authentification/passwordFirstConnexion'] !== ''

  // Does the user go to the log in page ?
  const goesToLogPage = ctx.route.name === 'utilisateur-login'

  // Does the user go to the log in page ?
  const goesToFirstConnexion = ctx.route.name === 'utilisateur-premiere-connexion'

  // Does the user go to the log in page ?
  const goesToLogout = ctx.route.name === 'utilisateur-logout'

  // If the user is logged :
  // If not : ALWAYS goes to the login page
  if (isLogged) {
    // A logged user is ALWAYS free to access the logout... but if he's going somewhere else :
    if (!goesToLogout) {
      // 1 - If the user is at his 1st connexion : ALWAYS goes to the page to set his password
      // 2 - If he tries to access a login / 1st connexion page : goes to /compte-client
      if (isFirstConnexion && !goesToFirstConnexion) {
        ctx.redirect(301, '/utilisateur/premiere-connexion')
      } else if (goesToLogPage || (!isFirstConnexion && goesToFirstConnexion)) {
        // We get the user's id
        const iduser = ctx.store.getters['authentification/idUser']
        ctx.redirect(301, '/account/' + iduser + '/tableau-de-bord')
      }
    }
  } else if (!goesToLogPage) {
    ctx.redirect(301, '/utilisateur/login')
  }
}
