export default function (ctx) {
  // If accessing the root path
  if (ctx.route.fullPath === '/') {
    // Is the user logged ?
    const isLogged = ctx.store.getters['authentification/isAuthenticated']

    // If logged in : new verification
    // Otherwise : to /utilisateur/login
    if (isLogged) {
      // We get the user's id
      const iduser = ctx.store.getters['authentification/idUser']

      // Is this the user's first connexion ?
      const isFirstConnexion = ctx.store.getters['authentification/passwordFirstConnexion'] !== ''

      // If it is the first connexion : goes to change password
      // Otherwise : goes to default page
      if (isFirstConnexion) {
        ctx.redirect(301, '/utilisateur/premiere-connexion')
      } else {
        // const iduser = JSON.parse(Cookies.get('user')).id
        ctx.redirect(301, '/account/' + iduser + '/tableau-de-bord')
      }
    } else {
      ctx.redirect(301, '/utilisateur/login')
    }
  }
}
