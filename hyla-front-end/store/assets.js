import axios from 'axios'
import config from '../config'

/** @param {String} path */
function api (path) {
  return config.API_URL + path
}

export const state = () => ({
  assets: [{
    assetID: '',
    assetName: '',
    assetOrganisation: '',
    running: false
  }],
  cost:
    [{
      date: '',
      amount: 0
    }]
})

export const getters = {
  getCost: stateAssets => function () {
    return stateAssets.cost
  },
  getAssets: (stateAssets) => {
    return stateAssets.assets
  },
  getAssetByID: stateAssets => (assetID) => {
    return stateAssets.assets.find(_ => _.assetID === assetID)
  }
}

export const actions = {
  /**
   * Get the total billing of an asset
   * @param commit
   * @param {String} assetID
   * @return {Promise<{err}>}
   */
  async getBilling ({ commit }, { assetID }) {
    try {
      const { data } = await axios.get(api('/assets/' + assetID + '/billing'))
      commit('updateBilling', data)
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
    return { err: false }
  },

  /**
   * Get the assets
   * @param commit
   * @param {Number} providerCredentialsID
   * @return {Promise<{err}>}
   */
  async fetchAssets ({ commit }, { providerCredentialsID }) {
    try {
      const { data } = await axios.get(api('/provider-credentials/' + providerCredentialsID + '/assets'))

      // We set the id of the provider's credentials into the assets
      data.forEach((asset) => { asset.idProvider_credentials = providerCredentialsID })

      commit('updateAssets', data)
      return data
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Get an asset given its Id
   * @param commit
   * @param {Number} providerCredentialsID
   * @param {Number} assetID
   * @return {Promise<{err}>}
   */
  async fetchAssetById ({ commit }, { providerCredentialsID, assetID }) {
    try {
      const { data } = await axios.get(api(`/provider-credentials/${providerCredentialsID}/assets/${assetID}`))
      commit('updateAsset', data)
      return data
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  }
}

export const mutations = {
  /**
   * Store in the state the billing of a given asset
   * @param stateAssets
   * @param {Object} data
   * @constructor
   */
  updateBilling (stateAssets, data) {
    stateAssets.cost = []
    data.forEach((element) => {
      stateAssets.cost.push(element)
    }
    )
  },

  /**
   * Store in the state the information of the assets
   * @param stateAssets
   * @param {Object} data
   * @constructor
   */
  updateAssets (stateAssets, data) {
    stateAssets.assets = []
    data.forEach((asset) => {
      mutations.updateAsset(stateAssets, asset)
    })
  },

  /**
   * Store in the state the information of an asset
   * @param stateAssets
   * @param {Object} asset
   * @constructor
   */
  updateAsset (stateAssets, asset) {
    // check if the asset already exists
    const existing = stateAssets.assets.findIndex(_ => _.assetID === asset.assetID)

    if (existing !== -1) {
      stateAssets.assets[existing] = asset
    } else {
      stateAssets.assets.push(asset)
    }
  }
}
