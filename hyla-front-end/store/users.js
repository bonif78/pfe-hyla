import axios from 'axios'
import config from '../config'

/** @param {String} path */
function api (path) {
  return config.API_URL + '/users' + path
}

export const state = () => ({
  users: [
    {
      id: -1,
      nameFirst: '',
      nameLast: '',
      email: '',
      role: {
        id: -1,
        name: ''
      }
    }
  ]
})

export const getters = {
  getUsers: stateUsers => function () {
    return stateUsers.users
  },
  getUserByID: stateUsers => (idUser) => {
    return stateUsers.users.find(_ => _.id === idUser)
  }
}

export const actions = {
  /**
   * Fetch all the Users of a global administrator
   * @param commit
   * @return {Promise<void>}
   */
  async fetchAllUsers ({ commit }) {
    const { data } = await axios.get(api(''))

    // UPDATE the list of users with new information
    commit('updateStateUsers', data)
  },

  /**
   * Fetch a User given its Id
   * @param commit
   * @param {String} userID
   * @return {Promise<void>}
   */
  async fetchUser ({ commit }, { userID }) {
    const { data } = await axios.get(api('/' + userID))

    // UPDATE the list of users with new information
    commit('updateStateUser', data)
  },

  /**
   * Create an new user
   * @param commit
   * @param {String} nameFirst
   * @param {String} nameLast
   * @param {String} email
   * @param {String} password
   * @param {Number} idRole
   * @return {Promise<{err}>}
   */
  async createUser ({ commit }, { nameFirst, nameLast, email, password, idRole }) {
    try {
      const { data } = await axios.post(api('/'), {
        nameFirst,
        nameLast,
        email,
        password: 'hyla7wgfth',
        role: {
          idrole: idRole
        }
      })
      commit('updateStateUser', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  },

  /**
   * Modify the role of the user
   * @param commit
   * @param {String} nameFirst
   * @param {String} nameLast
   * @param {Number} idUser
   * @param {String} email
   * @param {Number} idRole
   * @return {Promise<{err}>}
   */
  async modifyUserAccount ({ commit }, { idUser, nameFirst, nameLast, email, idRole }) {
    try {
      const { data } = await axios.put(api('/' + idUser + '/admin'), {
        nameFirst,
        nameLast,
        email,
        role: {
          idrole: idRole
        }
      })
      commit('updateStateUser', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  },

  /**
   * Delete a user from the app
   * @param {Number} idUser
   * @return {Promise<{err}>}
   */
  async deleteUser ({ commit }, { idUser }) {
    try {
      await axios.delete(api('/' + idUser))
      commit('deleteFromStateUser', idUser)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  }
}

export const mutations = {
  // Adds several users
  updateStateUsers (stateUsers, users) {
    // clear the state
    stateUsers.users = []

    // add each new user
    users.forEach((user) => {
      mutations.updateStateUser(stateUsers, user)
    })
  },

  // Adds one user
  updateStateUser (stateUsers, user) {
    // check if the user already exists
    const existing = stateUsers.users.findIndex(_ => _.id === user.idUser)
    const newUser = {
      id: user.idUser,
      nameFirst: user.nameFirst,
      nameLast: user.nameLast,
      email: user.email,
      role: {
        id: user.role.idrole,
        name: user.role.name
      }
    }
    if (existing !== -1) {
      stateUsers.users[existing] = newUser
      stateUsers.users = [...stateUsers.users]
    } else {
      stateUsers.users.push(newUser)
    }
  },

  // Remove a user from the state
  deleteFromStateUser (stateUsers, idUser) {
    const index = stateUsers.users.findIndex(_ => _.id === idUser)
    stateUsers.users.splice(index, 1)
  }
}
