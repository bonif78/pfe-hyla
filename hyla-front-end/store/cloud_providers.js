import axios from 'axios'
import config from '../config'

/** @param {String} path */
function api (path) {
  return config.API_URL + '/providers' + path
}

export const state = () => ({
  providers: [
    {
      id: -1,
      name: ''
    }
  ]
})

export const getters = {
  getProviders: (stateProviders) => { return stateProviders.providers },
  getProviderById: stateProviders => (idProvider) => {
    return stateProviders.providers.find(_ => _.id === idProvider)
  }
}

export const actions = {
  /**
   * Fetch all the Providers
   * @param commit
   * @return {Promise<void>}
   */
  async fetchAllProviders ({ commit }) {
    const { data } = await axios.get(api('/'))
    // UPDATE the list of providers with new information
    commit('updateStateProviders', data)
  },

  /**
   * Fetch a Provider given its ID
   * @param commit
   * @param {Number} idProvider
   * @return {Promise<void>}
   */
  async fetchProviderById ({ commit }, { idProvider }) {
    const { data } = await axios.get(api('/' + idProvider))
    // UPDATE the list of providers with new information
    commit('updateStateProvider', data)
  },

  /**
   * Create an new provider
   * @param commit
   * @param {String} name
   * @return {Promise<{err}>}
   */
  async createProvider ({ commit }, { name }) {
    try {
      const { data } = await axios.post(api('/'), { name })
      commit('updateStateProvider', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  },

  /**
   * Modify the provider
   * @param commit
   * @param {Number} idProvider
   * @param {String} name
   * @return {Promise<{err}>}
   */
  async modifyProvider ({ commit }, { idProvider, name }) {
    try {
      const { data } = await axios.put(api('/' + idProvider), { name })
      commit('updateStateProvider', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  },

  /**
   * Delete a provider from the app
   * @param {Number} idProvider
   * @return {Promise<{err}>}
   */
  async deleteProvider ({ commit }, { idProvider }) {
    try {
      const deleted = await axios.delete(api('/' + idProvider))
      commit('deleteFromStateProvider', idProvider)
      return { err: !deleted }
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  }
}

export const mutations = {
  updateStateProviders (stateProviders, providers) {
    // clear the state
    stateProviders.providers = []

    // add each new provider
    providers.forEach((provider) => {
      mutations.updateStateProvider(stateProviders, provider)
    })
  },
  updateStateProvider (stateProviders, provider) {
    // check if the provider already exists
    const existing = stateProviders.providers.findIndex(_ => _.id === provider.idprovider)
    const newProvider = {
      id: provider.idprovider,
      name: provider.name
    }
    if (existing !== -1) {
      stateProviders.providers[existing] = newProvider
    } else {
      stateProviders.providers.push(newProvider)
    }
  },
  deleteFromStateProvider (stateProviders, idProvider) {
    const index = stateProviders.providers.findIndex(_ => _.id === idProvider)
    stateProviders.providers.splice(index, 1)
  }
}
