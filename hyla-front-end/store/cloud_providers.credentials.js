import axios from 'axios'
import config from '../config'

/** @param {String} path */
function api (path) {
  return config.API_URL + '/provider-credentials' + path
}

export const state = () => ({
  providerCredentials: [
    {
      id: -1,
      username: '',
      email: '',
      description: '',
      provider: {
        id: -1,
        name: ''
      }
    }
  ]
})

export const getters = {
  getProviderCredentials: (statePCs) => { return statePCs.providerCredentials },
  getProviderCredentialById: statePCs => (idProviderCredential) => {
    return statePCs.providerCredentials.find(_ => _.id === idProviderCredential)
  }
}

export const actions = {
  /**
   * Fetch a ProviderCredential with its id
   * @param commit
   * @param {Number} id
   * @return {Promise<void>}
   */
  async fetchProviderCredentialById ({ commit }, { id }) {
    // We GET the data
    const { data } = await axios.get(api('/' + id))

    // UPDATE the list of provider credentials with new information
    commit('updateStateProviderCredential', data)
  },

  /**
   * Create an new provider credential
   * @param commit
   * @param {String} username
   * @param {String} email
   * @param {String} description
   * @param {String} password
   * @param {Number} idProvider
   * @return {Promise<{err}>}
   */
  async createProviderCredential ({ commit }, { username, email, description, password, idProvider }) {
    try {
      const { data } = await axios.post(api('/'), {
        username,
        email,
        description,
        password,
        provider: {
          idprovider: idProvider
        }
      })
      commit('updateStateProviderCredential', data)
      return data
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  },

  /**
   * Modify the provider credential
   * @param commit
   * @param {String} username
   * @param {String} email
   * @param {String} description
   * @param {Number} idProvider
   * @return {Promise<{err}>}
   */
  async modifyProviderCredential ({ commit }, { username, email, description, idProvider }) {
    try {
      const { data } = await axios.put(api('/' + idProvider), {
        username,
        email,
        description,
        provider: {
          idprovider: idProvider
        }
      })
      commit('updateStateProviderCredential', data)
      return data
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  },

  /**
   * Delete a provider credential from the app
   * @param {Number} idProvider
   * @return {Promise<{err}>}
   */
  async deleteCredentials ({ commit }, { idProvider }) {
    try {
      const deleted = await axios.delete(api('/' + idProvider))
      commit('deleteFromStatePC', idProvider)
      return { err: !deleted }
    } catch (err) {
      return { err: true, message: 'an error occured' }
    }
  }
}

export const mutations = {
  updateStateProviderCredential (statePCs, providerCredential) {
    // check if the provider credential already exists
    const existing = statePCs.providerCredentials.findIndex(_ => _.id === providerCredential.idprovider_credential)
    const newProvider = {
      id: providerCredential.idprovider_credential,
      username: providerCredential.username,
      email: providerCredential.email,
      description: providerCredential.description,
      provider: {
        id: providerCredential.provider.idprovider,
        name: providerCredential.provider.name
      }
    }
    if (existing !== -1) {
      statePCs.providerCredentials[existing] = newProvider
    } else {
      statePCs.providerCredentials.push(newProvider)
    }
  },

  deleteFromStatePC (statePCs, idProviderCredential) {
    const index = statePCs.providerCredentials.findIndex(_ => _.id === idProviderCredential)
    statePCs.providerCredentials.splice(index, 1)
  }
}
