import axios from 'axios'
import config from '../config'

/** @param {String} path */
function api (path) {
  return config.API_URL + '/teams' + path
}

export const state = () => ({
  teams: [
    {
      idequipe: -1,
      name: '',
      adminClient: {
        idUser: -1,
        nameFirst: '',
        nameLast: ''
      },
      users: [
        {
          idUser: -1,
          nameFirst: '',
          nameLast: ''
        }
      ],
      providerCredentials: [
        {
          idprovider_credential: -1,
          username: '',
          idcredentialext: ''
        }
      ]
    }
  ]
})

export const getters = {
  getAllTeams: stateTeams => function () {
    return stateTeams.teams
  },
  getTeamByID: stateTeams => (teamID) => {
    return stateTeams.teams.find(_ => _.idequipe === teamID)
  }
}

export const actions = {
  /**
   * Get the teams
   * @param commit
   * @return {Promise<{err}>}
   */
  async fetchAllTeams ({ commit }) {
    try {
      const { data } = await axios.get(api('/'))
      commit('updateTeams', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Get details about a team
   * @param commit
   * @param {String} teamID
   * @return {Promise<{err}>}
   */
  async fetchTeam ({ commit }, { teamID }) {
    try {
      const { data } = await axios.get(api('/' + teamID))
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * @param commit
   * @param {Number} userID
   * @return {Promise<{err}>}
   */
  async fetchTeamForUser ({ commit }, { userID }) {
    try {
      const { data } = await axios.get('/api/users/' + userID + '/teams')
      commit('updateTeams', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Create a new team
   * @param commit
   * @param {String} name
   * @param {Object} adminClient
   * @param {Object} users
   * @return {Promise<{err}>}
   */
  async createTeam ({ commit }, { name, adminClient, users }) {
    try {
      const { data } = await axios.post(api(''), { name, adminClient, users, providerCredentials: [] })
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Add many users to an existing team
   * @param commit
   * @param {Number} teamID
   * @param {Array} userIDs
   * @return {Promise<{err}>}
   */
  async addUsers ({ commit }, { teamID, userIDs }) {
    try {
      const { data } = await axios.put(api('/' + teamID + '/users'), userIDs)
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not added' }
    }
  },

  /**
   * Add a user to an existing team
   * @param commit
   * @param {Number} teamID
   * @param {Number} userID
   * @return {Promise<{err}>}
   */
  async addUser ({ commit }, { teamID, userID }) {
    try {
      const { data } = await axios.post(api('/' + teamID + '/user'), { idUser: userID })
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not added' }
    }
  },

  /**
   * Add a provider_credentials to an existing team
   * @param commit
   * @param {Number} teamID
   * @param {Number} providerID
   * @return {Promise<{err}>}
   */
  async addProviderCredentials ({ commit }, { teamID, providerID }) {
    try {
      const { data } = await axios.post(api('/' + teamID + '/credential'), { idprovider_credential: providerID })
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Modify a team
   * @param commit
   * @param {Number} teamID
   * @param {Number} adminID
   * @param {String} name
   * @return {Promise<{err}>}
   */
  async modifyTeam ({ commit }, { teamID, name, adminID }) {
    try {
      const { data } = await axios.put(api('/' + teamID), { name, adminClient: { idUser: adminID } })
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Modify the users of a team
   * @param commit
   * @param {Number} teamID
   * @param {Object} users
   * @return {Promise<{err}>}
   */
  async modifyTeamUser ({ commit }, { teamID, users }) {
    try {
      const { data } = await axios.put(api('/' + teamID + '/users'), { users })
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Modify the provider credentials of a team
   * @param commit
   * @param {Number} teamID
   * @param {Object} name
   * @param {Object} adminID
   * @return {Promise<{err}>}
   */
  async modifyTeamCredentials ({ commit }, { teamID, name, adminID }) {
    try {
      const { data } = await axios.put(api('/' + teamID + '/credentials'),
        { name, adminClient: { idUser: adminID } })
      commit('updateTeam', data)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  },

  /**
   * Delete a team
   * @param commit
   * @param {Number} teamID
   * @return {Promise<{err}>}
   */
  async deleteTeam ({ commit }, { teamID }) {
    try {
      await axios.delete(api('/' + teamID))
      commit('deleteTeam', teamID)
      return { err: false }
    } catch (err) {
      return { err: true, message: 'not fetch' }
    }
  }
}

export const mutations = {
  /**
   * Store in the state the information of the teams
   * @param stateTeams
   * @param {Object} data
   * @constructor
   */
  updateTeams (stateTeams, data) {
    // We reset the state
    stateTeams.teams = []

    // For each Team, we add it to the store
    data.forEach((team) => {
      mutations.updateTeam(stateTeams, team)
    })
  },

  /**
   * Store in the state the information of a team
   * @param stateTeams
   * @param {Object} team
   * @constructor
   */
  updateTeam (stateTeams, team) {
    // check if the user already exists
    const existing = stateTeams.teams.findIndex(_ => _.idequipe === team.idequipe)
    if (existing !== -1) {
      stateTeams.teams[existing] = team
    } else {
      stateTeams.teams.push(team)
    }

    stateTeams.teams = [...stateTeams.teams]
  },

  /**
   * Delete the information of a team from the store
   * @param stateTeams
   * @param {Number} teamID
   * @constructor
   */
  deleteTeam (stateTeams, teamID) {
    const index = stateTeams.teams.findIndex(_ => _.idequipe === teamID)
    stateTeams.teams.splice(index, 1)
  }
}
