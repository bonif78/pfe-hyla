import axios from 'axios'
import Cookies from 'js-cookie'
import config from '../config'

/** @param {String} path */
function api (path) {
  return config.API_URL + '/users' + path
}

export const state = () => ({})

export const getters = {
  idUser: () => parseInt(Cookies.get('idUser')) || -1,
  isAuthenticated: () => Cookies.get('idUser') !== undefined || false,
  passwordFirstConnexion: () => Cookies.get('passwordFirstConnexion') || ''
}

export const actions = {
  /**
   * Register the user with the credentials given
   * @param commit
   * @param {String} nameFirst
   * @param {String} nameLast
   * @param {String} email
   * @param {Number} roleID
   * @param {String} password
   * @param {String} passwordRepeat
   * @return {Promise<{err}>}
   */
  async register ({ commit }, { nameFirst, nameLast, email, roleID, password, passwordRepeat }) {
    if (password === passwordRepeat) {
      try {
        await axios.post(api('/'), { nameLast, nameFirst, email, password, role: { idrole: roleID } })
      } catch (err) {
        commit('accountError')
        return { err: true, message: 'not registered' }
      }
    } else {
      commit('accountError')
      return { err: true, message: 'password do not match' }
    }
    return { err: false }
  },

  /**
   * Log in the user with the credentials given
   * @param commit
   * @param {String} email
   * @param {String} password
   * @returns {Promise<{err}>}
   */
  async login ({ commit }, { email, password }) {
    try {
      const { data } = await axios.post(api('/login'), { email, password })
      commit('accountSuccess', data)

      // If the password begins by "hyla", then it is a first connexion !
      if (password.startsWith('hyla')) {
        commit('passwordFirstConnexion', { password })
      }
    } catch (err) {
      commit('accountError')
      return { err: true, message: 'not logged in' }
    }
    return { err: false }
  },

  /**
   * Log out the user
   * @param commit
   * @returns {Promise<void>}
   */
  logout ({ commit }) {
    try {
      commit('logoutUser')
    } catch (err) {
      commit('accountError')
    }
  },

  /**
   * modify nameFirst, nameLast and/or email of the user
   * @param commit
   * @param {String} nameFirst
   * @param {String} nameLast
   * @param {String} email
   * @returns {Promise<{err}>}
   */
  async modifyAccount ({ commit, getters }, { nameFirst, nameLast, email }) {
    try {
      // We get the user's id
      const userId = getters.idUser

      // We send our request
      await axios.put(api('/' + userId), { nameFirst, nameLast, email })
      // const data = response.data
      // commit('accountSuccess', { data })
    } catch (err) {
      return { err: true, message: 'not modified' }
    }
    return { err: false }
  },

  /**
   * modify password of the user
   * @param commit
   * @param {String} oldPassword
   * @param {String} newPassword
   * @returns {Promise<{err}>}
   * */
  async modifyPassword ({ commit, getters }, { oldPassword, newPassword }) {
    // We try to modify the password
    try {
      // We get the user's id
      const userId = getters.idUser

      // We send our request
      await axios.put(api('/' + userId + '/password'), { oldPassword, newPassword })
        .then(() => commit('passwordFirstConnexionValidated'))
    } catch (err) {
      return { err: true, message: 'not modified' }
    }
    return { err: false }
  }
}

export const mutations = {
  /**
 * Store that the user is logging in to a new account
 * @param stateUser
 * @param password
 */
  passwordFirstConnexion (stateUser, { password }) {
    // We set the cookie that this is his 1st connexion
    Cookies.set('passwordFirstConnexion', password)
  },

  /**
   * Store that the user has validated his account by setting a new password
   * @param stateUser
   */
  passwordFirstConnexionValidated (stateUser) {
    // We set the cookie that this is no longer the user's 1st connexion
    Cookies.remove('passwordFirstConnexion')
  },

  /**
   * Store in the state the information of the logged in user
   * @param stateUser
   * @param {Number} idUser
   * @param {String} nameFirst
   * @param {String} nameLast
   * @param {String} email
   * @constructor
   */
  accountSuccess (stateUser, { id, nameFirst, nameLast, email }) {
    // We set the cookie for the authentificated status
    Cookies.set('idUser', id)
  },

  /**
   * Disconnect the user
   * @param stateUser
   * @constructor
   */
  accountError (stateUser) {
    // We erase all the cookies
    Cookies.remove('idUser')
    Cookies.remove('passwordFirstConnexion')
  },

  /**
   * Take out the information regarding the user from the store
   * @param stateUser
   * @constructor
   */
  logoutUser (stateUser) {
    // We erase all the cookies
    Cookies.remove('idUser')
    Cookies.remove('passwordFirstConnexion')
  }
}
