import axios from 'axios'
import config from '../config'

/** @param {String} path */
function api (path) {
  return config.API_URL + path
}

export const state = () => ({
  rules: [{
    id: '-1',
    title: '',
    rule: '',
    active: false,
    idAsset: -1
  }]
})

export const getters = {
  getRules: (stateRules) => {
    return stateRules.rules
  },
  getRuleByID: stateRules => (id) => {
    return stateRules.rules.find(_ => _.id === id)
  }
}

export const actions = {
  /**
   * Get an asset's rules
   * @param commit
   * @param {Number} assetID
   * @return {Promise<{err}>}
   */
  async fetchRules ({ commit }, { assetID }) {
    try {
      const { data } = await axios.get(api('/assets/' + assetID + '/rules'))

      commit('updateRules', data)
      return data
    } catch (err) {
      return { err: true, message: 'not fetched' }
    }
  }
}

export const mutations = {
  /**
   * Store in the state the information of several assets' rules
   * @param stateAssets
   * @param {Object} data
   * @constructor
   */
  updateRules (stateRules, data) {
    stateRules.rules = []
    data.forEach((rule) => {
      mutations.updateRule(stateRules, rule)
    })
  },

  /**
   * Store in the state the information of an asset's rule
   * @param stateRules
   * @param {Object} rule
   * @constructor
   */
  updateRule (stateRules, rule) {
    // check if the rule already exists
    const existing = stateRules.rules.findIndex(_ => _.id === rule.id)

    if (existing !== -1) {
      stateRules.rules[existing] = rule
    } else {
      stateRules.rules.push(rule)
    }
  }
}
